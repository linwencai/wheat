import time
import threading
import datetime
import os
from PySide2.QtCore import QThreadPool, QRunnable, Signal, QObject
from main.models import Tasks, Session, SystemInfo, TaskLog, TaskReport
from main.utils import Constants
from main.utils.logger import Logger
from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.cidfonts import UnicodeCIDFont

pdfmetrics.registerFont(UnicodeCIDFont("STSong-Light"))


class TaskManager(object):
    _instance_lock = threading.Lock()

    def __new__(cls, *args, **kwargs):
        if not hasattr(TaskManager, "_instance"):
            with TaskManager._instance_lock:
                if not hasattr(TaskManager, "_instance"):
                    TaskManager._instance = object.__new__(cls)
        return TaskManager._instance

    def __init__(self):
        self._threadpool = QThreadPool.globalInstance()
        self._task_signal = TaskSignal()
        system_info = Session().query(SystemInfo).first()
        self._threadpool.setMaxThreadCount(system_info.number)
        self._task_signal.update_task.connect(self._save_task)
        self._all_runnables = []


    @staticmethod
    def instance():
        if not hasattr(TaskManager, "_instance"):
            return TaskManager()
        else:
            return TaskManager._instance

    """
    添加任务
    """
    def add_task(self, task: Tasks, report_dir, log_dir):
        self._save_task(task)
        logger = self._create_log(task.id, task.tiffFile, log_dir)
        logger.debug_to_file("Adding {} to analyse".format(os.path.basename(task.tiffFile)))
        task_runnable = TaskRunnable(task, task_signal=self._task_signal, report_dir=report_dir, log_dir=log_dir, logger=logger)
        self._threadpool.start(task_runnable)
        self._all_runnables.append(task_runnable)
    """
    新增或更新任务
    """
    def _save_task(self, task):

        with Session.begin() as sb:
            sb.add(task)

        if task.state == Constants.State.CANCEL \
                or task.state == Constants.State.COMPLETE\
                or task.state == Constants.State.IDLE:
            for runnable in self._all_runnables:
                if runnable.task.id == task.id:
                    self._all_runnables.remove(runnable)
    """
    取消任务，只能取消Idle或Queuing状态的任务
    """
    def cancel_task(self, task_id):
        for runnable in self._all_runnables:
            if runnable.task.id == task_id and (runnable.task.state == Constants.State.IDLE
                                                or runnable.task.state == Constants.State.QUEUING):
                self._threadpool.cancel(runnable)
                runnable.task.state = Constants.State.CANCEL
                self._save_task(runnable.task)
                self._wirte_cancel_log(task_id)

    def _wirte_cancel_log(self, task_id):
        taskLog = Session().query(TaskLog).filter_by(taskId=task_id).first()
        if taskLog:
            logger = Logger(log_file_path=taskLog.logPath)
            logger.debug_to_file("The task has been cancelled")

    def _create_log(self, task_id, tiffFile, log_dir):
        timestamp = int(datetime.datetime.now().timestamp() * 1000)
        file_base = os.path.splitext(tiffFile)[0]
        file_base = file_base.replace(" ", "_")
        dir_name = os.path.basename(file_base) + "_" + str(timestamp)
        entire_log_dir = os.path.join(log_dir, dir_name)
        if not os.path.exists(entire_log_dir):
            os.makedirs(entire_log_dir)
        log_file_path = os.path.join(entire_log_dir, "task_log.txt")
        task_log = TaskLog(taskId=task_id, logPath=log_file_path)
        with Session.begin() as sb:
            sb.add(task_log)
        logger = Logger(log_file_path=log_file_path)
        return logger


class TaskSignal(QObject):
    update_task = Signal(Tasks)

class TaskRunnable(QRunnable):
    def __init__(self, task: Tasks, task_signal: TaskSignal, report_dir, log_dir, logger):
        super(TaskRunnable, self).__init__()
        self.task = task
        self.logger = logger
        self.task.state = Constants.State.QUEUING
        self.task_signal = task_signal
        self.logger.debug_to_file("The {} is queuing".format(os.path.basename(task.tiffFile)))
        self.task_signal.update_task.emit(self.task)
        self.report_dir = report_dir
        self.log_dir = log_dir


    def run(self):
        if self.task.state == Constants.State.CANCEL:
            return
        self.task.startTime = int(datetime.datetime.now().timestamp())
        self.task.state = Constants.State.EXTRACTING_DATA
        self.logger.debug_to_file("Start extracting data")
        self.task_signal.update_task.emit(self.task)
        time.sleep(20)
        self.task.state = Constants.State.ANALYSING_DATA
        self.logger.debug_to_file("Start analysing data")
        self.task_signal.update_task.emit(self.task)
        time.sleep(20)
        self.generate_report(self.task.tiffFile, self.report_dir, self.task.inspectionItem,self.task.id)
        self.task.state = Constants.State.COMPLETE
        self.logger.debug_to_file("The analysis task has been completed")
        self.task.endTime = int(datetime.datetime.now().timestamp())
        self.task.duration = self.task.endTime - self.task.startTime
        self.task_signal.update_task.emit(self.task)

    def generate_report(self, csv_file, output_dir, disease_type,taskId):
        if disease_type == Constants.InspectionItem.SARCOIDOSIS:
            if not os.path.exists(output_dir):
                os.makedirs(output_dir)
            file_base = os.path.splitext(csv_file)[0]
            file_name = os.path.basename(file_base)
            file_name = file_name.replace(" ", "_")
            pdf_file = os.path.join(output_dir, file_name+"_"+str(self.task.id)+".pdf")
            pdf = canvas.Canvas(pdf_file, bottomup=0, initialFontName="STSong-Light", initialFontSize=14)
            pdf.drawString(100, 60, "肺结节良恶性分析")
            pdf.drawString(100, 100, "良性结节参考范围：[0-0.5)")
            pdf.drawString(100, 130, "恶性结节参考范围：[0.5-1]")
            pdf.drawString(100, 160, "风险得分：0.003")
            pdf.drawString(100, 190, "预测结果：良性")
            pdf.showPage()
            pdf.save()

            report_task = TaskReport(taskId=taskId,reportPath=pdf_file)
            with Session.begin() as sb:
                sb.add(report_task)

        return True



