import os
from main.utils import Constants
from main.models import Tasks, Session, TaskLog
from main.BaseActivity import BaseActivity
from PySide2.QtWidgets import QLabel, QTextBrowser

# 实现分析日志会话框
class AnalysisLogActivity(BaseActivity):
    def __init__(self, taskId: int):
        ui_filename = self.resourcePath("layout/analysis_log_layout.ui")
        super().__init__(ui_filename)
        self._taskId = taskId
        self.filename_label: QLabel = self.window.findChild(QLabel, 'filename_label')
        self.creator_label: QLabel = self.window.findChild(QLabel, 'creator_label')
        self.chip_label: QLabel = self.window.findChild(QLabel, 'chip_label')
        self.progress_label: QLabel = self.window.findChild(QLabel, 'progress_label')
        self.batch_label: QLabel = self.window.findChild(QLabel, 'batch_label')
        self.project_label: QLabel = self.window.findChild(QLabel, 'project_label')
        self.logRecord_textBrowser: QTextBrowser = self.window.findChild(QTextBrowser,"textBrowser")

        self.init_ui()

    def init_ui(self):
        if self._taskId:
            task = Session().query(Tasks).filter(Tasks.id == self._taskId).first()
            file_name = os.path.basename(task.tiffFile)
            self.filename_label.setText(file_name)
            self.creator_label.setText(task.operator)
            self.chip_label.setText(task.chipVer)
            state = task.state
            if state == Constants.State.IDLE or state == Constants.State.QUEUING:
                self.progress_label.setText("排队中")
            elif state == Constants.State.EXTRACTING_DATA:
                self.progress_label.setText("数据提取")
            elif state == Constants.State.ANALYSING_DATA:
                self.progress_label.setText("检验分析")
            elif state == Constants.State.COMPLETE:
                self.progress_label.setText("完成")
            elif state == Constants.State.FAILURE:
                self.progress_label.setText("失败")
            elif state == Constants.State.CANCEL:
                self.progress_label.setText("已取消")
            else:
                pass
            self.batch_label.setText(task.batch)
            self.project_label.setText(task.inspectionItem)
            taskLog = Session().query(TaskLog).filter(TaskLog.taskId == self._taskId).first()
            if taskLog:
                log_file = taskLog.logPath
                flag = os.path.exists(log_file)
                if not flag:
                    self.logRecord_textBrowser.append("日志文件不存在，可能已被删除！")
                else:
                    with open(log_file, "r") as file:
                        while True:
                            line = file.readline()
                            if line:
                                self.logRecord_textBrowser.append(line)
                            else:
                                break
