import sys
import os
import datetime
from main.utils.Constants import ChipVer
from main.BaseActivity import BaseActivity
from main.BaseWidget import BaseWidget
from main import models
from main.utils.Constants import InspectionItem, State
from PySide2.QtWidgets import QPushButton, QLabel, QListWidget, QListWidgetItem, \
    QRadioButton, QComboBox, QFileDialog, QWidget, QHBoxLayout, QMessageBox, QStyledItemDelegate
from PySide2.QtGui import QIcon, QPixmap, QImage, QFont
from PySide2.QtCore import QSize, Signal, QObject
from main.task.TaskManager import TaskManager
from main.MessageDialog import MessageDialog


class AddTaskActivity(BaseWidget):
    def __init__(self):
        ui_filename = self.resourcePath("layout/add_task_layout.ui")
        super().__init__(ui_filename)
        self.sarcoidosisRadioButton: QRadioButton = self.window.findChild(QRadioButton, "sarcoidosisRadioButton")
        self.dmRadioButton: QRadioButton = self.window.findChild(QRadioButton, "dmRadioButton")
        self.chipComboBox: QComboBox = self.window.findChild(QComboBox, "chipComboBox")
        self.chipComboBox.setItemDelegate(QStyledItemDelegate())
        style = self.chipComboBox.styleSheet()
        style = style.replace("../res/ic_down_arrow.png", self.resourcePath("res/ic_down_arrow.png"))
        self.chipComboBox.setStyleSheet(style)
        sarcoidosisStyle = self.sarcoidosisRadioButton.styleSheet()
        sarcoidosisStyle = sarcoidosisStyle.replace("../res/ic_check_box_checked.png", self.resourcePath("res/ic_check_box_checked.png"))
        self.sarcoidosisRadioButton.setStyleSheet(sarcoidosisStyle)
        dmStyle = self.dmRadioButton.styleSheet()
        dmStyle = dmStyle.replace("../res/ic_check_box_checked.png",
                                                    self.resourcePath("res/ic_check_box_checked.png"))
        self.dmRadioButton.setStyleSheet(dmStyle)

        self.tiffButton: QPushButton = self.window.findChild(QPushButton, "tiffButton")
        # self.tiffButton.setIcon(QIcon(self.resourcePath("res/ic_upload.png")))
        # self.tiffButton.setIconSize(QSize(16, 16))
        self.tiffButton.clicked.connect(self.onClick)
        self.listWidget: QListWidget = self.window.findChild(QListWidget, "listWidget")
        self.startButton: QPushButton = self.window.findChild(QPushButton, "startButton")
        self.startButton.clicked.connect(self.onStart)
        self.tiffTipLabel: QLabel = self.window.findChild(QLabel, "tiffTipLabel")
        self.tiffTipLabel.setHidden(True)
        self.tiff_files = []
        self.chipComboBox.addItems([ChipVer.V13, ChipVer.V16])
        self.init_list_widget()
        self.signal = TaskSignal()
        self.msgDialog = MessageDialog()

    def onClick(self):
        system_info: models.SystemInfo = models.Session().query(models.SystemInfo).first()
        tiff_path = QFileDialog.getOpenFileName(None, "选择TIFF文件", system_info.sourceDir, "tiff files (*.tiff)")
        tiff_file_path = tiff_path[0]
        if tiff_file_path and len(tiff_file_path) > 0:
            exist_item = self.find_item(tiff_file_path)
            if exist_item:
                self.show_msg_box("文件已存在。")
            else:
                self.tiff_files.append(tiff_file_path)
                self.add_tiff_item(tiff_file_path)

    def find_item(self, tiff_file):
        for i in range(len(self.tiff_files)):
            item_widget: ItemWidget = self.listWidget.itemWidget(self.listWidget.item(i))
            if item_widget.tiff_file == tiff_file:
                return item_widget
        return None

    def _clear_processed_tiff(self):
        for i in range(len(self.tiff_files)):
            item_widget: ItemWidget = self.listWidget.itemWidget(self.listWidget.item(0))
            if not item_widget.checkmark_label.isHidden():
                self.tiff_files.remove(self.tiff_files[0])
                self.listWidget.model().removeRow(0)
            else:
                break
    def _clear_all(self):
        self.tiff_files.clear()
        self.listWidget.clear()


    def add_tiff_item(self, tiff_path):
        list_item = QListWidgetItem()
        list_item.setSizeHint(QSize(200, 30))
        item_widget = ItemWidget(tiff_path)
        item_widget.del_button.tag = list_item
        item_widget.del_button.clicked.connect(self.del_tiff_item)
        self.listWidget.addItem(list_item)
        self.listWidget.setItemWidget(list_item, item_widget)

    def del_tiff_item(self):
        item: MyPushButton = self.sender()
        self.listWidget.model().removeRow(self.listWidget.row(item.tag))
        self.tiff_files.remove(item.tiff_file)

    def init_list_widget(self):
        self.listWidget.verticalScrollBar().setStyleSheet("""
            QScrollBar:vertical {
                border: none;
                width: 8px;
                margin: 22px 0 22px 0;
            }
            QScrollBar::handle:vertical {
                width: 8px;
                background-color: #00000000;
                border-radius: 4px;
            }
            QScrollBar::handle:vertical:hover {
                width: 8px;
                background-color: #50000000;
                border-radius: 4px;
            }
            QScrollBar::add-line:vertical {
                background: transparent;
                height: 20px;
                subcontrol-position: bottom;
                subcontrol-origin: margin;
            }

            QScrollBar::sub-line:vertical {
                background: transparent;
                height: 20px;
                subcontrol-position: top;
                subcontrol-origin: margin;
            }
            QScrollBar::up-arrow:vertical,QScrollBar::down-arrow:vertical {
                width: 3px;
                height: 3px;
                background: transparent;
            }

            QScrollBar::add-page:vertical,QScrollBar::sub-page:vertical {
                background: none;
            }
        """)

    def onStart(self):
        inspection_item = ""
        if self.sarcoidosisRadioButton.isChecked():
            inspection_item = InspectionItem.SARCOIDOSIS
        elif self.dmRadioButton.isChecked():
            inspection_item = InspectionItem.DM
        chip_ver = self.chipComboBox.currentText()
        if len(self.tiff_files) == 0:
            self.tiffTipLabel.setHidden(False)
        else:
            batch = self.get_last_batch()

            system_info: models.SystemInfo = models.Session().query(models.SystemInfo).first()
            for i in range(len(self.tiff_files)):
                item_widget = self.listWidget.itemWidget(self.listWidget.item(i))
                item_widget.checkmark_label.setHidden(False)
                task = models.Tasks()
                task.state = State.IDLE
                task.chipVer = chip_ver
                task.inspectionItem = inspection_item
                task.operator = BaseActivity.logined_user
                task.tiffFile = self.tiff_files[i]
                task.batch = batch
                task.createTime = int(datetime.datetime.today().timestamp())
                TaskManager.instance().add_task(task, report_dir=system_info.resultDir, log_dir=system_info.logDir)

            self.signal.update_signal.emit(True)
            self.show_msg_box("已新增{}个分析任务，可前往分析进度页面查看详情。".format(len(self.tiff_files)))
            self._clear_all()

    def show_msg_box(self, text):
        self.msgDialog.setMsg(text)
        self.msgDialog.show()

    def get_last_batch(self):
        today = datetime.date.today()
        todayStr = today.strftime("%Y%m%d")
        todayTime = int(datetime.datetime(today.year, today.month, today.day, 0, 0, 0).timestamp())
        task = models.Session().query(models.Tasks)\
            .filter(models.Tasks.createTime >= todayTime)\
            .order_by(models.Tasks.createTime.desc())\
            .first()
        if task:
            oldBatch = task.batch
            batchNum = int(oldBatch.split("-")[1])
            newBatch = todayStr + "-" + str(batchNum + 1)
            return newBatch
        else:
            return todayStr + "-1"

class TaskSignal(QObject):
    update_signal = Signal(bool)

class MyPushButton(QPushButton):
    def __init__(self):
        super(MyPushButton, self).__init__()
        self.tag = None
        self.tiff_file = None

class ItemWidget(QWidget):
    def __init__(self, tiff_file: str):
        super(ItemWidget, self).__init__()
        self.tiff_file = tiff_file
        hlayout = QHBoxLayout()
        tiff_name = os.path.basename(tiff_file)
        tiff_name_label = QLabel(tiff_name)
        self.checkmark_label = QLabel()
        self.checkmark_label.setFixedSize(15, 13)
        self.checkmark_label.setScaledContents(True)
        self.checkmark_label.setPixmap(QPixmap(BaseActivity.resourcePath("res/ic_checkmark.png")))
        self.checkmark_label.setStyleSheet("background-color: #00000000")
        tiff_name_label.setStyleSheet("font-family:Microsoft YaHei;background-color: transparent;font-size: 10px;color: #4A4A4A;")
        self.del_button = MyPushButton()
        self.del_button.setText("删除")
        self.del_button.setStyleSheet("font-family:Microsoft YaHei;font-size: 10px;color: #4A90E2;background-color:transparent;")
        self.del_button.setFixedSize(QSize(24, 24))
        self.del_button.tiff_file = tiff_file
        self.del_button.setFlat(True)
        hlayout.addWidget(self.checkmark_label)
        hlayout.addWidget(tiff_name_label)
        hlayout.addWidget(self.del_button)
        hlayout.setContentsMargins(20, 5, 20, 5)
        self.window().setLayout(hlayout)
