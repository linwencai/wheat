import hashlib
import re

from PySide2.QtCore import QObject, Signal, Qt, QEvent
from PySide2.QtGui import QIcon, QColor, QPixmap
from PySide2.QtWidgets import QLineEdit, QPushButton, QLabel, QComboBox, QFrame, QGraphicsDropShadowEffect
from main import models
from main.BaseActivity import BaseActivity
from main.MessageDialog import MessageDialog
from main.utils.Constants import UserRole, UserStatus


class AddAccountActivity(BaseActivity):
    """账户管理-新增账户"""

    def __init__(self):
        ui_filename = self.resourcePath("layout/add_account_layout.ui")
        super().__init__(ui_filename)
        self.window.setWindowModality(Qt.ApplicationModal)
        self.window.setWindowFlags(Qt.FramelessWindowHint)
        self.window.setAttribute(Qt.WA_TranslucentBackground)
        self.base_frame = self.window.findChild(QFrame, "baseFrame")
        self.close_button = self.window.findChild(QPushButton, "closeButton")
        self.close_button.setIcon(QIcon(self.resourcePath("res/ic_del.png")))
        self.close_button.clicked.connect(self.close)
        self.confirm_button = self.window.findChild(QPushButton, "confirmButton")
        self.confirm_button.clicked.connect(self.on_confirm)
        self.nickname_edit = self.window.findChild(QLineEdit, "nicknameEdit")
        self.realname_edit = self.window.findChild(QLineEdit, "realnameEdit")
        self.phoneNumber_edit = self.window.findChild(QLineEdit, "phoneNumberEdit")
        self.role_combo_box = self.window.findChild(QComboBox, "roleComboBox")
        style = self.role_combo_box.styleSheet()
        style = style.replace("../res/ic_down_arrow.png", self.resourcePath("res/ic_down_arrow.png"))
        self.role_combo_box.setStyleSheet(style)
        self.account_tip_label = self.window.findChild(QLabel, "accountTipLabel")
        self.name_tip_label = self.window.findChild(QLabel, "nameTipLabel")
        self.phone_tip_label = self.window.findChild(QLabel, "phoneTipLabel")
        self.role_tip_label = self.window.findChild(QLabel, "roleTipLabel")
        self.required_label1 = self.window.findChild(QLabel, "requiredLabel1")
        self.required_label2 = self.window.findChild(QLabel, "requiredLabel2")
        self.required_label3 = self.window.findChild(QLabel, "requiredLabel3")
        self.required_label4 = self.window.findChild(QLabel, "requiredLabel4")
        required_icon = QPixmap(self.resourcePath("res/bitian.png"))
        required_icon.setDevicePixelRatio(3.0)
        self.required_label1.setPixmap(required_icon)
        self.required_label2.setPixmap(required_icon)
        self.required_label3.setPixmap(required_icon)
        self.required_label4.setPixmap(required_icon)
        self.close_button.installEventFilter(self)

        self.role_combo_box.setCurrentIndex(-1)
        self.role_combo_box.setPlaceholderText("请选择")
        self.signal = AddAccountSignal()

        self.setShadow()

    def setShadow(self):
        shadow = QGraphicsDropShadowEffect(self.base_frame)
        shadow.setColor(QColor(0, 0, 0, 80))
        shadow.setOffset(0, 0)
        shadow.setBlurRadius(10)
        self.base_frame.setGraphicsEffect(shadow)

    def on_confirm(self):
        account_name = self.nickname_edit.text()
        real_name = self.realname_edit.text()
        phone_number = self.phoneNumber_edit.text()
        role = self.role_combo_box.currentText()

        self.account_tip_label.setText("")
        self.name_tip_label.setText("")
        self.phone_tip_label.setText("")
        self.role_tip_label.setText("")

        if account_name.strip() == "" or real_name.strip() == "" or phone_number.strip() == "" or role.strip() == "":
            if account_name.strip() == "":
                self.account_tip_label.setText("必填项，不能为空")
            if real_name.strip() == "":
                self.name_tip_label.setText("必填项，不能为空")
            if phone_number.strip() == "":
                self.phone_tip_label.setText("必填项，不能为空")
            if role.strip() == "":
                self.role_tip_label.setText("必填项，不能为空")
        else:
            with models.Session.begin() as sb:
                user = sb.query(models.User).filter_by(nickname=account_name).first()
            flag = True
            if account_name:
                if user:
                    self.account_tip_label.setText("该账户名已被使用")
                    flag = False
                elif len(account_name) < 6 or len(account_name) > 15:
                    self.account_tip_label.setText("账户名长度不得低于6位，高于15位")
                    flag = False
                elif not re.search(r"^[a-z][a-z0-9_]{5,14}$", account_name):
                    self.account_tip_label.setText('账户名只能由"小写字母、数字、_”组成，且以字母开头')
                    flag = False
            if real_name:
                if len(real_name) > 8:
                    self.name_tip_label.setText("姓名长度不能超过8个字！")
                    flag = False
            if phone_number:
                if not re.search(r"^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$",
                                 phone_number):
                    self.phone_tip_label.setText("手机号码格式错误，请输入有效手机号码！")
                    flag = False

            if flag:
                with models.Session.begin() as sb:
                    if role == "管理员":
                        role = UserRole.ADMINISTRATOR
                    else:
                        role = UserRole.GENERAL
                    init_pwd = hashlib.md5((account_name+phone_number[-4:]).encode("utf8")).hexdigest()
                    add_user = models.User(nickname=account_name, password=init_pwd, realname=real_name,
                                           phoneNumber=phone_number, role=role, status=UserStatus.INACTIVE)
                    sb.add(add_user)

                    user = sb.query(models.User).filter_by(nickname=account_name).first()
                self.show_msg_box("新增帐户成功，帐户ID为{0}，账户名为{1}，初始密码为：账户名+手机号后4位".format(user.id, account_name))
                self.close()
                self.signal.update_signal.emit(True)

    def show_msg_box(self, text):
        msg_box = MessageDialog()
        msg_box.setTitle("新增成功")
        msg_box.setMsg(text)
        msg_box.exec()

    def eventFilter(self, watched: QObject, event: QEvent):
        if watched == self.close_button:
            if event.type() == QEvent.HoverEnter:
                self.close_button.setIcon(QIcon(self.resourcePath("res/ic_white_close.png")))
            elif event.type() == QEvent.HoverLeave:
                self.close_button.setIcon(QIcon(self.resourcePath("res/ic_del.png")))
        return False


class AddAccountSignal(QObject):
    update_signal = Signal(bool)

