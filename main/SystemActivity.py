import os
import sys

from PySide2.QtCore import QObject, QEvent
from main import models
from multiprocessing import cpu_count
from PySide2.QtWidgets import QLabel, QPushButton, QSpinBox, QFileDialog, QApplication, QWidget, \
    QGraphicsDropShadowEffect, QGridLayout
from main.BaseActivity import BaseActivity
from PySide2.QtGui import Qt, QColor, QIcon



class SystemActivity(BaseActivity):

    def __init__(self):
        ui_filename = self.resourcePath("layout/system_setting_layout.ui")
        print(ui_filename)
        super().__init__(ui_filename)
        # 读取ui对象
        self.resultDir_button: QPushButton = self.window.findChild(QPushButton, "resultfile_button")
        self.resultpath_label: QLabel = self.window.findChild(QLabel, "resultpath_label")
        self.sourceDir_button: QPushButton = self.window.findChild(QPushButton, "sourcefile_button")
        self.sourcepath_label: QLabel = self.window.findChild(QLabel, "sourcepath_label")
        self.sum_spinBox: QSpinBox = self.window.findChild(QSpinBox, "sum_spinBox")
        self.logpath_label: QLabel = self.window.findChild(QLabel, "logpath_label")
        self.logDir_button: QPushButton = self.window.findChild(QPushButton, 'logfile_button')
        self.range_label: QLabel = self.window.findChild(QLabel, "rangeLabel")
        self.myWidget:QWidget = self.window.findChild(QWidget,"myWidget")
        self.baseWidget = self.window.findChild(QWidget,"baseWidget")
        self.closeButton = self.window.findChild(QPushButton,"closeButton")
        self.closeButton.setIcon(QIcon(self.resourcePath("res/ic_del.png")))
        self.closeButton.clicked.connect(self.close)
        self.closeButton.installEventFilter(self)
        # 设置窗口属性
        self.window.setWindowModality(Qt.ApplicationModal)
        self.window.setWindowFlags(Qt.FramelessWindowHint)
        self.window.setAttribute(Qt.WA_DeleteOnClose)
        self.window.setAttribute(Qt.WA_TranslucentBackground, True)
        maximum = cpu_count()*2
        minimum = 1
        self.range_label.setText("({0}-{1}):".format(minimum, maximum))
        # 最大值为cpu数量
        self.sum_spinBox.setMaximum(maximum)
        self.sum_spinBox.setMinimum(minimum)
        style = self.sum_spinBox.styleSheet()
        style = style.replace("../res/ic_up_arrow.png", self.resourcePath("res/ic_sys_up_arrow.png"))
        style = style.replace("../res/ic_down_arrow.png", self.resourcePath("res/ic_sys_down_arrow.png"))
        self.sum_spinBox.setStyleSheet(style)
        with models.Session.begin() as ses:
            sysinfo: models.SystemInfo = ses.query(models.SystemInfo).first()
        if sysinfo.sourceDir:
            source_dir = sysinfo.sourceDir.replace("\\","/")
            self.sourcepath_label.setText(source_dir)

        result_dir = sysinfo.resultDir.replace("\\","/")
        log_dir = sysinfo.logDir.replace("\\","/")
        self.resultpath_label.setText(result_dir)
        self.logpath_label.setText(log_dir)

        self.sum_spinBox.setValue(sysinfo.number)

        # 点击实现读取目录路径信号
        self.resultDir_button.clicked.connect(self.onResultDir)
        self.sourceDir_button.clicked.connect(self.onSourceDir)
        self.logDir_button.clicked.connect(self.onLogDir)
        # 计数器变化信号
        self.sum_spinBox.valueChanged.connect(self.onNum)

        # 实现阴影
        self.base()
        self.addShadow()

    def base(self):
        self.baseLayout = QGridLayout()
        self.baseLayout.addWidget(self.myWidget)
        self.baseWidget.setLayout(self.baseLayout)
        self.baseWidget.setAttribute(Qt.WA_TranslucentBackground)
        self.myWidget.setFixedSize(884,502)

    def addShadow(self):
        shadow = QGraphicsDropShadowEffect(self)
        shadow.setOffset(0,0)
        shadow.setBlurRadius(10)
        shadow.setColor(QColor(163,163,163))
        self.myWidget.setGraphicsEffect(shadow)

    def onSourceDir(self):
        """影像文件目录"""
        sourcepath_str = QFileDialog.getExistingDirectory(dir='C:/')
        if len(sourcepath_str) > 0:
            sourcepath_str = os.path.abspath(sourcepath_str)
            sourcepath_UI = sourcepath_str.replace("\\","/")
            self.sourcepath_label.setText(sourcepath_UI)
            # 路径保存到数据库
            with models.Session.begin() as sb:
                sys = sb.query(models.SystemInfo).first()
                sys.sourceDir = sourcepath_str

    def onResultDir(self):
        '''分析报告目录'''
        resultpath_str = QFileDialog.getExistingDirectory(dir='C:/')
        if len(resultpath_str) > 0:
            resultpath_str = os.path.abspath(resultpath_str)
            # todo:更换目录后任务list里报告打开按钮无法找到报告文件?
            # 方案1：移动report文件夹到新更换的目录(如果不存在reports前提)下
            # 方案2：model添加新表TaskReport,建立reportFilePath字段，打开报告按钮点击的时候读取此路径
            resultpath_UI = resultpath_str.replace("\\",'/')
            self.resultpath_label.setText(resultpath_UI)
            with models.Session.begin()as sb:
                sys = sb.query(models.SystemInfo).first()
                sys.resultDir = resultpath_str


    def onLogDir(self):
        '''日志'''
        logpath_str = QFileDialog.getExistingDirectory(dir='C:/')
        if len(logpath_str) > 0:
            logpath_str = os.path.abspath(logpath_str)
            logpath_UI = logpath_str.replace("\\","/")
            self.logpath_label.setText(logpath_UI)
            with models.Session.begin() as sb:
                sys = sb.query(models.SystemInfo).first()
                sys.logDir = logpath_str


    def onNum(self):
        """实现数字更新保存到数据库"""
        with models.Session.begin() as sb:
            sys = sb.query(models.SystemInfo).first()
            sys.number = self.sum_spinBox.value()



    def eventFilter(self, watched: QObject, event: QEvent):
        if watched == self.closeButton:
            if event.type() == QEvent.HoverEnter:
                self.closeButton.setIcon(QIcon(self.resourcePath("res/ic_white_close.png")))
            elif event.type() == QEvent.HoverLeave:
                self.closeButton.setIcon(QIcon(self.resourcePath("res/ic_del.png")))
        return False


if __name__ == '__main__':
    models.init_db()
    models.init_admin()
    app = QApplication(sys.argv)
    system_window = SystemActivity()
    system_window.show()
    app.exec_()
