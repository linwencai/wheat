import sys
from sqlalchemy import or_
from main import models
from main.AboutActivity import AboutActivity
from main.AccountInfoActivity import AccountInfoActivity
from main.AccountManageActivity import AccountManageActivity
from main.BaseActivity import BaseActivity
from main.AddTaskActivity import AddTaskActivity
from main.MessageDialog import MessageDialog
from main.utils import Constants
from main.EditLicenseActivity import EditLicenseActivity
from main.ResetPwdActivity import ResetPwdActivity
from main.TaskListActivity import TaskListActivity
from main.SystemActivity import SystemActivity
from PySide2.QtCore import Qt, QSize, QObject, QEvent, QSettings, Signal
from PySide2.QtWidgets import QFrame, QGraphicsDropShadowEffect, QPushButton, QStackedWidget, QRadioButton, QLabel, \
    QVBoxLayout, QListWidget, QListWidgetItem, QStyledItemDelegate, QStyleOptionViewItem
from PySide2.QtGui import QColor, QPixmap, QIcon

from main.utils.Constants import State, UserRole


class MainWindowActivity(BaseActivity):
    def __init__(self):
        ui_filename = self.resourcePath("layout/main_window_layout.ui")
        super().__init__(ui_filename)

        self.mousePress = False
        self.mousePoint = None

        self.addTaskActivity = None
        self.taskListActivity = None
        self.accountManageActivity = None
        self.accountInfoActivity = None
        self.resetPwdActivity = None
        self.systemInfoActivity = None
        self._editLicenseActivity = None
        self.aboutInfoActivity = None

        self.window.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowSystemMenuHint | Qt.WindowMinimizeButtonHint)
        self.stackedWidget: QStackedWidget = self.window.findChild(QStackedWidget, "stackedWidget")
        self.setStackedWidgetShadow()
        self.topFrame: QFrame = self.window.findChild(QFrame, "topFrame")
        self.setShadow()
        self.bigLogoLabel:QLabel = self.window.findChild(QLabel,"bigLogoLabel")
        self.bigLogoLabel.setPixmap(QPixmap(self.resourcePath("res/iclogo.jpg")))
        self.bigLogoLabel.setScaledContents(True)
        self.uploadButton: QRadioButton = self.window.findChild(QRadioButton, "uploadButton")
        self.uploadButton.clicked.connect(self.onUpload)
        self.initUploadButton()
        self.allButton: QRadioButton = self.window.findChild(QRadioButton, "allButton")
        self.allButton.clicked.connect(self.onTaskProgress)
        self.allButton.click()
        self.waitButton: QRadioButton = self.window.findChild(QRadioButton, "waitButton")
        self.waitButton.clicked.connect(self.onTaskProgress)
        self.analysingButton: QRadioButton = self.window.findChild(QRadioButton, "analysingButton")
        self.analysingButton.clicked.connect(self.onTaskProgress)
        self.successButton: QRadioButton = self.window.findChild(QRadioButton, "successButton")
        self.successButton.clicked.connect(self.onTaskProgress)
        self.failureButton: QRadioButton = self.window.findChild(QRadioButton, "failureButton")
        self.failureButton.clicked.connect(self.onTaskProgress)
        self.cancelButton: QRadioButton = self.window.findChild(QRadioButton, "cancelButton")
        self.cancelButton.clicked.connect(self.onTaskProgress)
        self.accountManageButton: QRadioButton = self.window.findChild(QRadioButton, "accountManageButton")
        self.init_account_manage_button()
        self.accountManageButton.clicked.connect(self.onAccountManage)

        self.minimizeButton: QPushButton = self.window.findChild(QPushButton, "minimizeButton")
        self.minimizeButton.setIcon(QIcon(self.resourcePath("res/ic_minimize.png")))
        self.minimizeButton.clicked.connect(self.window.showMinimized)
        self.closeButton: QPushButton = self.window.findChild(QPushButton, "closeButton")
        self.closeButton.setIcon(QIcon(self.resourcePath("res/ic_del.png")))
        self.closeButton.clicked.connect(self.onClose)

        self.accountButton: QPushButton = self.window.findChild(QPushButton, "accountButton")
        self.accountButton.clicked.connect(self.onAccountSetting)
        self.accountFrame: QFrame = self.window.findChild(QFrame, "accountFrame")
        self.accountFrame.setVisible(False)
        self.accountListWidget: QListWidget = self.window.findChild(QListWidget, "accountSettingListWidget")
        self.delegate = ItemDelegate()
        self.accountListWidget.setItemDelegate(self.delegate)
        self.accountListWidget.itemClicked.connect(self.onAccountListWidgetItem)

        self.settingButton: QPushButton = self.window.findChild(QPushButton, "settingButton")
        self.settingButton.setIcon(QIcon(self.resourcePath("res/ic_setting.png")))
        self.settingButton.setIconSize(QSize(21, 24))
        self.settingButton.clicked.connect(self.onSetting)
        self.settingListWidget: QListWidget = self.window.findChild(QListWidget, "settingListWidget")
        self.setSettingShadow()
        self.settingListWidget.setVisible(False)
        self.settingListWidget.itemClicked.connect(self.onListWidgetItem)
        self._batchsignal = BatchSignal()
        self._batchsignal.hideSignal.connect(self.onHideBatch)


        self.init_account_set()
        self.topFrame.installEventFilter(self)
        self.closeButton.installEventFilter(self)
        self.window.installEventFilter(self)


    def onHideBatch(self):
        if self.taskListActivity.dateEdit_frame.isVisible():
            self.taskListActivity.dateEdit_frame.setVisible(False)
            self.taskListActivity.batch_button.setStyleSheet(self.taskListActivity.sty)

    def init_account_manage_button(self):
        current_name = BaseActivity.logined_user
        if current_name:
            with models.Session.begin() as sb:
                user = sb.query(models.User).filter_by(nickname=current_name).first()
            if user.role == UserRole.ADMINISTRATOR:
                self.accountManageButton.setVisible(True)
            else:
                self.accountManageButton.setVisible(False)

    def initUploadButton(self):
        iconLabel = QLabel()
        icPixmap = QPixmap(self.resourcePath("res/ic_upload.png"))
        icPixmap.setDevicePixelRatio(3.0)
        iconLabel.setPixmap(icPixmap)
        iconLabel.setAlignment(Qt.AlignHCenter)
        iconLabel.setStyleSheet("background-color: transparent;")
        textLabel = QLabel("上传文件")
        textLabel.setAlignment(Qt.AlignHCenter)
        textLabel.setStyleSheet("background-color: transparent;font-size: 12px;color: #525252;")
        vboxLayout = QVBoxLayout()
        vboxLayout.addWidget(iconLabel)
        vboxLayout.addWidget(textLabel)
        vboxLayout.setSpacing(5)
        vboxLayout.setMargin(0)
        vboxLayout.setAlignment(Qt.AlignVCenter)
        self.uploadButton.setLayout(vboxLayout)

    def setShadow(self):
        shadow = QGraphicsDropShadowEffect(self.topFrame)
        shadow.setColor(QColor(0, 0, 0, 7))
        shadow.setYOffset(3.0)
        shadow.setBlurRadius(10)
        self.topFrame.setGraphicsEffect(shadow)

    def setStackedWidgetShadow(self):
        shadow = QGraphicsDropShadowEffect(self.stackedWidget)
        shadow.setColor(QColor(163, 163, 163, 50))
        shadow.setOffset(0, 3)
        shadow.setBlurRadius(10)
        self.stackedWidget.setGraphicsEffect(shadow)

    def setSettingShadow(self):
        shadow = QGraphicsDropShadowEffect(self.settingListWidget)
        shadow.setColor(QColor(163, 163, 163, 50))
        shadow.setOffset(0, 3)
        shadow.setBlurRadius(10)
        self.settingListWidget.setGraphicsEffect(shadow)
        shadow = QGraphicsDropShadowEffect(self.accountFrame)
        shadow.setColor(QColor(163, 163, 163, 50))
        shadow.setOffset(0, 3)
        shadow.setBlurRadius(10)
        self.accountFrame.setGraphicsEffect(shadow)

    def init_account_set(self):
        user = models.Session().query(models.User).filter_by(nickname=BaseActivity.logined_user).first()
        if user:
            firstname = user.realname[0]
            self.accountButton.setText(firstname)
            text = "{}\n{}".format(user.realname, user.phoneNumber)
            item1 = QListWidgetItem(text, self.accountListWidget)
            item1.setIcon(QIcon(self.resourcePath("res/ic_next_step.png")))
            item1.setSizeHint(QSize(30, 48))
            item2 = QListWidgetItem("修改密码", self.accountListWidget)
            item2.setSizeHint(QSize(30, 28))
            item3 = QListWidgetItem("退出登录", self.accountListWidget)
            item3.setSizeHint(QSize(30, 28))

    def onSetting(self):
        visiable = self.settingListWidget.isVisible()
        self.settingListWidget.setVisible(not visiable)

    def onAccountSetting(self):
        visiable = self.accountFrame.isVisible()
        self.accountFrame.setVisible(not visiable)

    def onListWidgetItem(self):
        self.settingListWidget.setVisible(False)
        row = self.settingListWidget.currentRow()
        if row == 0:
            winX = self.window.pos().x()
            winY = self.window.pos().y()
            self.systemInfoActivity = SystemActivity()
            self.systemInfoActivity.window.move(winX+70, 133+winY)
            self.systemInfoActivity.show()
        elif row == 1:
            self._editLicenseActivity = EditLicenseActivity()
            self._editLicenseActivity.show()
        else:
            self.aboutInfoActivity = AboutActivity()
            # self.aboutInfoActivity.show()
        self.settingListWidget.clearSelection()

    def onAccountListWidgetItem(self):
        self.accountFrame.setVisible(False)
        row = self.accountListWidget.currentRow()
        if row == 0:
            win_x = self.window.pos().x()
            win_y = self.window.pos().y()
            self.accountInfoActivity = AccountInfoActivity()
            self.accountInfoActivity.window.move(win_x+267, win_y+30)
            self.accountInfoActivity.signal.update_signal.connect(self.update_account_info)
            self.accountInfoActivity.show()
        elif row == 1:
            win_x = self.window.pos().x()
            win_y = self.window.pos().y()
            self.resetPwdActivity = ResetPwdActivity()
            self.resetPwdActivity.window.move(win_x+185, win_y+107)
            self.resetPwdActivity.show()

        else:
            with models.Session.begin() as sb:
                tasks = sb.query(models.Tasks).filter(or_(models.Tasks.state == State.EXTRACTING_DATA,
                                                          models.Tasks.state == State.ANALYSING_DATA)).all()
            if tasks:
                self.notice = MessageDialog()
                self.notice.setTitle("错误提示")
                self.notice.setMsg("还有未完成的分析任务，不能退出登录！")
                self.notice.exec()
            else:
                self.window.close()
                from main.LoginActivity import LoginActivity
                self.loginActivity = LoginActivity()
                self.loginActivity.show()
                setting = QSettings("C:/ProgramData/icarbonx/config/config.ini", QSettings.IniFormat)
                setting.setValue('authlogin', False)
        self.accountListWidget.clearSelection()
    """
    当修改账户信息成功后，会调用该方法
    """
    def update_account_info(self, flag: bool):
        user = models.Session().query(models.User).filter_by(nickname=BaseActivity.logined_user).first()
        if user:
            text = "{}\n{}".format(user.realname, user.phoneNumber)
            self.accountListWidget.item(0).setText(text)

    def onUpload(self):
        if not self.addTaskActivity:
            self.addTaskActivity = AddTaskActivity()
            self.stackedWidget.addWidget(self.addTaskActivity)
        self.stackedWidget.setCurrentWidget(self.addTaskActivity)

    def onTaskProgress(self):
        radioButton = self.sender()
        status = radioButton.property("status")  # 在radiobutton添加的动态自定义的属性
        title = "全部文件"
        if status == 0:
            statusList = [Constants.State.IDLE, Constants.State.QUEUING, Constants.State.EXTRACTING_DATA,
                          Constants.State.ANALYSING_DATA, Constants.State.COMPLETE, Constants.State.CANCEL,
                          Constants.State.FAILURE]
            title = "全部文件"
        elif status == 1:
            statusList = [Constants.State.IDLE, Constants.State.QUEUING]
            title = "等待分析"
        elif status == 2:
            statusList = [Constants.State.EXTRACTING_DATA, Constants.State.ANALYSING_DATA]
            title = "正在分析"
        elif status == 3:
            statusList = [Constants.State.COMPLETE]
            title = "分析成功"
        elif status == 4:
            statusList = [Constants.State.FAILURE]
            title = "分析失败"
        elif status == 5:
            statusList = [Constants.State.CANCEL]
            title = "取消分析"
        if not self.taskListActivity:
            self.taskListActivity = TaskListActivity(status=statusList)
            self.stackedWidget.addWidget(self.taskListActivity)
        else:
            self.taskListActivity.changeQueryStatus(status=statusList)
        self.taskListActivity.setTitleText(title)
        self.stackedWidget.setCurrentWidget(self.taskListActivity)

    def onAccountManage(self):
        if not self.accountManageActivity:
            self.accountManageActivity = AccountManageActivity()
            self.stackedWidget.addWidget(self.accountManageActivity)
        self.stackedWidget.setCurrentWidget(self.accountManageActivity)

    def onClose(self):
        with models.Session.begin() as sb:
            tasks = sb.query(models.Tasks).filter(or_(models.Tasks.state == State.EXTRACTING_DATA,
                                                      models.Tasks.state == State.ANALYSING_DATA)).all()
        if tasks:
            self.notice = MessageDialog()
            self.notice.setTitle("错误提示")
            self.notice.setMsg("还有未完成的分析任务，不能退出系统！")
            self.notice.exec()
        else:
            self.notice = MessageDialog()
            self.notice.setTitle("退出提示")
            self.notice.setMsg("你确定要退出吗？")
            self.notice.setBtn("确定")
            self.notice.exec()

    def eventFilter(self, watched: QObject, event: QEvent):
        if watched == self.topFrame:
            if event.type() == QEvent.MouseButtonPress:
                self.mousePress = True
                self.mousePoint = event.globalPos() - self.window.pos()

            elif event.type() == QEvent.MouseMove:
                if self.mousePress:
                    self.window.move(event.globalPos() - self.mousePoint)

            elif event.type() == QEvent.MouseButtonRelease:
                self.mousePress = False
        elif watched == self.window:
            if event.type() == QEvent.MouseButtonRelease:
                if self.settingListWidget.isVisible():
                    self.settingListWidget.setVisible(False)
                if self.accountFrame.isVisible():
                    self.accountFrame.setVisible(False)
                self._batchsignal.hideSignal.emit(True)

        elif watched == self.closeButton:
            if event.type() == QEvent.HoverEnter:
                self.closeButton.setIcon(QIcon(self.resourcePath("res/ic_white_close.png")))
            elif event.type() == QEvent.HoverLeave:
                self.closeButton.setIcon(QIcon(self.resourcePath("res/ic_del.png")))
        return False


class ItemDelegate(QStyledItemDelegate):
    def paint(self, painter, option, index):
        option.decorationPosition = QStyleOptionViewItem.Right
        super(ItemDelegate, self).paint(painter, option, index)

class BatchSignal(QObject):
    hideSignal = Signal(bool)



if __name__ == '__main__':
    mainWindowActivity = MainWindowActivity()
    mainWindowActivity.show()
    sys.exit(BaseActivity.app.exec_())