import re

from PySide2.QtCore import Qt, QObject, Signal, QEvent
from PySide2.QtGui import QIcon, QColor, QPixmap
from PySide2.QtWidgets import QLineEdit, QPushButton, QLabel, QFrame, QGraphicsDropShadowEffect
from main.BaseActivity import BaseActivity
from main import models
from main.utils.Constants import UserRole


class AccountInfoActivity(BaseActivity):
    def __init__(self):
        ui_filename = self.resourcePath("layout/account_info_layout.ui")
        super().__init__(ui_filename)
        self.window.setWindowModality(Qt.ApplicationModal)
        self.window.setWindowFlags(Qt.FramelessWindowHint)
        self.window.setAttribute(Qt.WA_TranslucentBackground)
        self.base_frame = self.window.findChild(QFrame, "baseFrame")
        self.close_button = self.window.findChild(QPushButton, "closeButton")
        self.close_button.setIcon(QIcon(self.resourcePath("res/ic_del.png")))
        self.close_button.clicked.connect(self.close)
        self.id = self.window.findChild(QLineEdit, "IDEdit")
        self.nickname = self.window.findChild(QLineEdit, "nicknameEdit")
        self.name = self.window.findChild(QLineEdit, "realnameEdit")
        self.phone = self.window.findChild(QLineEdit, "phoneEdit")
        self.role = self.window.findChild(QLineEdit, "roleEdit")
        self.status = self.window.findChild(QLineEdit, "statusEdit")
        self.phone_tip_label = self.window.findChild(QLabel, "phoneTipLabel")
        self.required_label = self.window.findChild(QLabel, "requiredLabel")
        required_icon = QPixmap(self.resourcePath("res/bitian.png"))
        required_icon.setDevicePixelRatio(3.0)
        self.required_label.setPixmap(required_icon)
        self.confirm_button = self.window.findChild(QPushButton, "confirmButton")
        self.confirm_button.clicked.connect(self.on_confirm)
        self.signal = AccountInfoSignal()
        self.close_button.installEventFilter(self)

        self.setShadow()
        self.query_account_info()

    def setShadow(self):
        shadow = QGraphicsDropShadowEffect(self.base_frame)
        shadow.setColor(QColor(0, 0, 0, 80))
        shadow.setOffset(0, 0)
        shadow.setBlurRadius(10)
        self.base_frame.setGraphicsEffect(shadow)

    def query_account_info(self):
        current_name = BaseActivity.logined_user
        if current_name:
            with models.Session.begin() as sb:
                user = sb.query(models.User).filter_by(nickname=current_name).first()
                id = user.id
                nickname = user.nickname
                name = user.realname
                phone = user.phoneNumber
                role = user.role
                if role == UserRole.ADMINISTRATOR:
                    role = "管理员"
                elif role == UserRole.GENERAL:
                    role = "普通用户"
                status = user.status

            self.id.setText(str(id))
            self.nickname.setText(nickname)
            self.name.setText(name)
            self.phone.setText(phone)
            self.role.setText(role)
            self.status.setText(status)

    def on_confirm(self):
        phone_number = self.phone.text()
        current_name = BaseActivity.logined_user
        # 校验并更新手机号
        if re.search(r"^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$", phone_number):
            with models.Session.begin() as sb:
                user = sb.query(models.User).filter_by(nickname=current_name).first()
                user.phoneNumber = phone_number
            self.close()
            self.signal.update_signal.emit(True)
        else:
            self.phone_tip_label.setText("手机号码格式有误，请重新输入")
            self.phone.clear()

    def eventFilter(self, watched: QObject, event: QEvent):
        if watched == self.close_button:
            if event.type() == QEvent.HoverEnter:
                self.close_button.setIcon(QIcon(self.resourcePath("res/ic_white_close.png")))
            elif event.type() == QEvent.HoverLeave:
                self.close_button.setIcon(QIcon(self.resourcePath("res/ic_del.png")))
        return False


class AccountInfoSignal(QObject):
    update_signal = Signal(bool)





