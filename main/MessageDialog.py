import sys
from main.BaseActivity import BaseActivity
from PySide2.QtWidgets import QLabel, QPushButton, QGraphicsDropShadowEffect, QFrame
from PySide2.QtCore import Qt, QObject, Signal
from PySide2.QtGui import QColor, QIcon


class MessageDialog(BaseActivity):
    def __init__(self):
        ui_filename = self.resourcePath("layout/message_dialog_layout.ui")
        super().__init__(ui_filename)
        self.backFrame: QFrame = self.window.findChild(QFrame, "backFrame")
        self.msgLable: QLabel = self.window.findChild(QLabel, "msgLabel")
        self.titleLabel: QLabel = self.window.findChild(QLabel, "titleLabel")
        self.okButton: QPushButton = self.window.findChild(QPushButton, "okButton")
        self.okButton.setText('知道了')
        self.noButton: QPushButton = self.window.findChild(QPushButton, "noButton")
        self.noButton.setHidden(True)
        self.noButton.clicked.connect(self.onNo)
        self.closeButton: QPushButton = self.window.findChild(QPushButton, "closeButton")
        self.closeButton.setIcon(QIcon(self.resourcePath("res/ic_close.png")))
        self.okButton.clicked.connect(self.onOk)
        self.closeButton.clicked.connect(self.onOk)
        self.window.setWindowFlags(Qt.FramelessWindowHint)
        self.window.setAttribute(Qt.WA_TranslucentBackground, True)
        self._text = None
        self.setShadow()
        self.signal = UpdateAccountSignal()
        self.window.setWindowModality(Qt.ApplicationModal)

    def setShadow(self):
        shadow = QGraphicsDropShadowEffect(self.backFrame)
        shadow.setColor(QColor(0, 0, 0, 80))
        shadow.setOffset(0, 0)
        shadow.setBlurRadius(10)
        self.backFrame.setGraphicsEffect(shadow)

    def onOk(self):

        Btn :QPushButton = self.sender()
        BtnText = Btn.text()
        #判断关闭本窗口还是退出app
        if BtnText == "继续":
            self.window.close()
            self.signal.update_signal.emit(True)
        elif BtnText == "确定":
            BaseActivity.app.exit()
        else:
            self.window.close()

    def onNo(self):
        self.window.close()

    def setMsg(self, msg: str):
        self.msgLable.setText(msg)

    def setTitle(self, title: str):
        self.titleLabel.setText(title)

    def setBtn(self, text: str):

        self._text = text
        self.noButton.setHidden(False)
        self.okButton.setText(self._text)


class UpdateAccountSignal(QObject):
    update_signal = Signal(bool)


if __name__ == '__main__':
    mainWindowActivity = MessageDialog()
    mainWindowActivity.show()
    sys.exit(BaseActivity.app.exec_())