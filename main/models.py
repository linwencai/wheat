import datetime
import os
import hashlib
from sqlalchemy import Column, String, Integer, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from main.utils.Constants import UserRole
from multiprocessing import cpu_count

db_file = "wheat.db"
db_dir = os.path.join("C:\\", "ProgramData", "wheat", "dbstore")
db_path = os.path.join(db_dir, db_file)
Base = declarative_base()
engine = create_engine("sqlite:///"+db_path, echo=False,connect_args={'check_same_thread': False})
Session = sessionmaker(engine, expire_on_commit=False)
db_code = 1


def init_db():
    is_exist = os.path.exists(db_path)
    if not is_exist:
        if not os.path.exists(db_dir):
            os.makedirs(db_dir)
        Base.metadata.create_all(engine)
        with Session.begin() as sb:
            sb.add(DBEntity(code=1))
    else:
        with Session.begin() as sb:
            entity = sb.query(DBEntity).first()
            if entity:
                if entity.code < db_code:
                    Base.metadata.create_all(engine)
                    entity.code = db_code


def init_admin():
    with Session.begin() as sb:
        result = sb.query(User).count()
        if result == 0:
            default_name = "admin"
            default_pass = hashlib.md5("admin".encode("utf8")).hexdigest()
            sb.add(User(nickname=default_name, password=default_pass, realname="Jolly", role=UserRole.ADMINISTRATOR))

class DBEntity(Base):
    __tablename__ = "dbentity"
    id = Column(Integer, primary_key=True)
    #数据库版本号
    code = Column(Integer, default=1)


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    #用户昵称
    nickname = Column(String)
    #密码
    password = Column(String)
    #用户名
    realname = Column(String)
    #用户手机号
    phoneNumber = Column(String)
    #用户角色
    role = Column(Integer)
    #用户状态
    status = Column(String)
    # 更新时间
    update_at = Column(DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now)


class SystemInfo(Base):
    __tablename__ = "systeminfos"
    id = Column(Integer, primary_key=True)
    # 影像文件路径
    sourceDir = Column(String)
    # 分析结果路径
    resultDir =Column(String, default="C:\\ProgramData\\icarbonx\\reports")
    # 一次分析多少数量
    number = Column(Integer, default= int(cpu_count() / 3))
    # 操作日志目录
    logDir = Column(String, default="C:\\ProgramData\\icarbonx\\logs")


class Tasks(Base):
    __tablename__ = "tasks"
    id = Column(Integer, primary_key=True)
    #检测项目
    inspectionItem = Column(String)
    #芯片版本
    chipVer = Column(String)
    #TIFF文件，保存的是绝对路径
    tiffFile = Column(String)
    #创建人
    operator = Column(String)
    #状态
    state = Column(String)
    #批次(20210409-1)
    batch = Column(String)
    #开始时间(时间戳，精确到秒)
    startTime = Column(Integer)
    #结束时间(时间戳，精确到秒)
    endTime = Column(Integer)
    #持续时间(精确到秒)
    duration = Column(Integer)
    # 创建时间(时间戳，精确到秒)
    createTime = Column(Integer)

    def __str__(self):

        return {"id": self.id, "inspectionItem": self.inspectionItem, "chipVer": self.chipVer,
                "tiffFile": self.tiffFile, "operator": self.operator, "state": self.state,
                "batch": self.batch, "startTime": self.startTime, "endTime": self.endTime,
                "duration": self.duration}.__str__()


class TaskLog(Base):
    __tablename__ = "tasklog"
    id = Column(Integer, primary_key=True)
    #对应的task id
    taskId = Column(Integer)
    # log文件绝对路径
    logPath = Column(String)

class TaskReport(Base):
    __tablename__ = "taskreport"

    id = Column(Integer,primary_key=True)
    #对应的task id
    taskId = Column(Integer)
    reportPath = Column(String)






class LicenseEntity(Base):
    __tablename__ = "license"
    id = Column(Integer, primary_key=True)
    # 授权码
    licenseCode = Column(String)


#获取所有任务
def get_all_tasks():
    all_tasks = None
    with Session.begin() as sb:
        all_tasks = sb.query(Tasks).all()
    return all_tasks















































