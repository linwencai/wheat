import hashlib
import sys, re
from PySide2.QtCore import QEvent, Qt, QObject
from PySide2.QtGui import QColor, QIcon, QPixmap

from main import models
from main.BaseActivity import BaseActivity
from PySide2.QtWidgets import QLineEdit, QPushButton, QLabel,QGraphicsDropShadowEffect, QFrame
from main.MessageDialog import MessageDialog
from main.utils.Constants import UserStatus


class ResetPwdActivity(BaseActivity):

    def __init__(self):
        ui_filename = self.resourcePath("layout/reset_pwd_layout.ui")
        super().__init__(ui_filename)
        self.old_pwd_line: QLineEdit = self.window.findChild(QLineEdit, 'old_pwd_line')
        self.new_pwd_line: QLineEdit = self.window.findChild(QLineEdit, 'new_pwd_line')
        self.confirm_pwd_line: QLineEdit = self.window.findChild(QLineEdit, 'confirm_pwd_line')
        self.first_login_label: QLabel = self.window.findChild(QLabel, 'first_login_label')
        self.old_pwd_label: QLabel = self.window.findChild(QLabel, 'old_pwd_label')
        self.new_pwd_label: QLabel = self.window.findChild(QLabel, 'new_pwd_label')
        self.confirm_pwd_label: QLabel = self.window.findChild(QLabel, 'confirm_pwd_label')
        self.closeButton: QPushButton = self.window.findChild(QPushButton, 'closeButton')
        self.closeButton.setIcon(QIcon(self.resourcePath("res/ic_del.png")))

        self.label16 = self.window.findChild(QLabel,"label_16")
        self.label17 = self.window.findChild(QLabel,"label_17")
        self.label18 = self.window.findChild(QLabel,"label_18")
        bitianIcon = QPixmap(self.resourcePath("res/bitian.png"))
        bitianIcon.setDevicePixelRatio(3.0)
        self.label16.setPixmap(bitianIcon)
        self.label17.setPixmap(bitianIcon)
        self.label18.setPixmap(bitianIcon)

        self.confirm_button: QPushButton = self.window.findChild(QPushButton, 'confirm_button')
        self.backFrame = self.window.findChild(QFrame,"myframe")

        self.window.setWindowFlags(Qt.FramelessWindowHint)
        self.window.setAttribute(Qt.WA_TranslucentBackground)
        self.window.setWindowModality(Qt.ApplicationModal)
        self.window.setWindowIcon(QIcon(self.resourcePath("res/ic_logo.jpg")))
        self.window.setWindowTitle('修改密码')

        self.resetSucessMessage = MessageDialog()
        self.logined_password = BaseActivity.logined_pwd
        self.logined_username = BaseActivity.logined_user
        session = models.Session()
        user: models.User = session.query(models.User).filter_by(nickname=self.logined_username).first()
        self.oldPwd = user.password

        # 首次登录判断
        if user.status != UserStatus.INACTIVE:
            self.first_login_label.setVisible(False)

        else:
            self.first_login_label.setVisible(True)
            self.window.installEventFilter(self)

        self.closeButton.clicked.connect(self.close)
        self.closeButton.installEventFilter(self)
        self.confirm_button.clicked.connect(self.onConfirm)

        self.setShadow()

    def setShadow(self):
        shadow = QGraphicsDropShadowEffect(self.backFrame)
        shadow.setColor(QColor(0, 0, 0, 80))
        shadow.setOffset(0, 0)
        shadow.setBlurRadius(10)
        self.backFrame.setGraphicsEffect(shadow)

    def eventFilter(self, watched: QObject, event:QEvent):
        if watched == self.window and event.type() == QEvent.Close:
            BaseActivity.app.quit()

        elif watched == self.closeButton:
            if event.type() == QEvent.HoverEnter:
                self.closeButton.setIcon(QIcon(self.resourcePath("res/ic_white_close.png")))

            elif event.type() == QEvent.HoverLeave:
                self.closeButton.setIcon(QIcon(self.resourcePath("res/ic_del.png")))

        return False



    def onConfirm(self):
        old_pwd: str = self.old_pwd_line.text().strip()
        old_hmpwd = hashlib.md5(old_pwd.encode("utf8")).hexdigest()
        new_pwd: str = self.new_pwd_line.text().strip()
        new_hmpwd = hashlib.md5(new_pwd.encode("utf8")).hexdigest()

        confirm_pwd: str = self.confirm_pwd_line.text().strip()
        confirm_hmpwd = hashlib.md5(confirm_pwd.encode("utf8")).hexdigest()
        self.old_pwd_label.setText('')
        self.new_pwd_label.setText('')
        self.confirm_pwd_label.setText('')
        # 空校验
        if old_pwd == '' or new_pwd == '' or confirm_pwd == '':
            if old_pwd == '':
                self.old_pwd_label.setText('请填写旧密码')
            if new_pwd == '':
                self.new_pwd_label.setText('请填写新密码')
            if confirm_pwd == '':
                self.confirm_pwd_label.setText('请填写确认密码')
            return
        # 校验密码
        if old_hmpwd != self.oldPwd:
            self.old_pwd_label.setText('旧密码输入有误')
            return
        if not 8 <= len(new_pwd) <= 20:
            self.new_pwd_label.setText("密码长度不得低于8位，高于20位。")
            return
        if old_hmpwd == new_hmpwd:
            self.new_pwd_label.setText("新密码不能与旧密码相同")
            return
        re_rule = r'^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9~!@#%&_]{8,20}$'
        result = re.search(re_rule, new_pwd)
        if not result:
            self.new_pwd_label.setText("密码必须包含大小写字母、数字")
            return
        if confirm_hmpwd != new_hmpwd:
            self.confirm_pwd_label.setText("两次输入新密码不相同")
            return
        # 更新密码
        with models.Session.begin() as ses:
            userModel: models.User = ses.query(models.User).filter_by(nickname=self.logined_username).first()
            if userModel.status == UserStatus.INACTIVE:
                userModel.password = confirm_hmpwd
                userModel.status = UserStatus.ACTIVE
            else:
                userModel.password = confirm_hmpwd
        # 修改成功提示
        with models.Session.begin() as ses:
            userModel: models.User = ses.query(models.User).filter_by(nickname=self.logined_username).first()
            if userModel.password == confirm_hmpwd:
                self.resPwdDialog()
                self.window.removeEventFilter(self)
                self.window.close()
    # 提示对话框
    def resPwdDialog(self):
        self.resetSucessMessage.setMsg("密码修改成功！")
        self.resetSucessMessage.setTitle('成功提示')
        self.resetSucessMessage.exec()


if "__main__" == __name__:

    resetPwdActivity = ResetPwdActivity()
    resetPwdActivity.show()
    sys.exit(BaseActivity.app.exec_())


