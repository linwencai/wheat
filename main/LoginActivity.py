import sys
import hashlib

from PySide2.QtCore import QSettings, QTimer, Qt, QObject, QEvent
from PySide2.QtNetwork import QLocalSocket, QLocalServer

from main.MainWindowActivity import MainWindowActivity
from main.BaseActivity import BaseActivity
from main import models
from main.MessageDialog import MessageDialog
from main.ResetPwdActivity import ResetPwdActivity
from main.utils import licenseUtil
from PySide2.QtWidgets import QLineEdit, QPushButton, QLabel, QWidget, QPlainTextEdit, QCheckBox
from PySide2.QtGui import QPixmap, QIcon

from main.utils.AESUtil import encrypt, decrypt
from main.utils.Constants import UserStatus


class LoginActivity(BaseActivity):
    def __init__(self):
        ui_filename = self.resourcePath("layout/login_layout.ui")
        super().__init__(ui_filename)
        self.closeButton = self.window.findChild(QPushButton,"closeButton")
        self.closeButton.setIcon(QIcon(self.resourcePath("res/ic_del.png")))
        self.closeButton.clicked.connect(self.app.quit)
        self.minimizeButton = self.window.findChild(QPushButton,"minimizeButton")
        self.minimizeButton.setIcon(QIcon(self.resourcePath("res/ic_minimize.png")))
        self.minimizeButton.clicked.connect(self.window.showMinimized)
        self.welcomeLabel: QLabel = self.window.findChild(QLabel, "welcomeLabel")
        self.welcomeLabel.setPixmap(QPixmap(self.resourcePath("res/welcome.jpg")))
        self.welcomeLabel.setScaledContents(True)
        self.accountname_edit = self.window.findChild(QLineEdit, "accountEdit")
        self.pwd_edit = self.window.findChild(QLineEdit, "pwdEdit")
        self.login_button = self.window.findChild(QPushButton, "loginButton")
        self.logintip_label = self.window.findChild(QLabel, "loginTipLabel")
        self.login_button.clicked.connect(self.onLogin)
        self.licenseWidget = self.window.findChild(QWidget, "licenseWidget")
        self.licenseWidget.setHidden(True)
        self.validLicense = True
        self.licenseCode_edit: QPlainTextEdit = self.window.findChild(QPlainTextEdit, "licenseCodeEdit")
        self.licenseTip_label: QLabel = self.window.findChild(QLabel, "licenseTipLabel")
        self.verify_button: QPushButton = self.window.findChild(QPushButton, "verifyButton")
        self.verify_button.clicked.connect(self.onVerify)
        self.licenseTip_label.setHidden(True)
        self.indicator:QCheckBox = self.window.findChild(QCheckBox,'checkBox')
        indicator_style = self.indicator.styleSheet()
        indicator_style = indicator_style.replace("../res/checkbox.png", self.resourcePath("res/checkbox.png"))
        self.indicator.setStyleSheet(indicator_style.replace("../res/check.svg",self.resourcePath("res/check.svg")))
        self.window.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowSystemMenuHint | Qt.WindowMinimizeButtonHint)
        self.initSavedInfo()

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.onGoAuthLogin)
        self.timer.setSingleShot(True)
        self.timer.start(500)

        self.closeButton.installEventFilter(self)

    def onLogin(self):
        if not self.validLicense:
            self.licenseTip_label.setHidden(False)
            return
        account_name = self.accountname_edit.text()
        pwd = self.pwd_edit.text()

        if account_name.strip() == "" or pwd.strip() == "":
            self.logintip_label.setText("用户名和密码不能为空")
        else:
            hex_pwd = hashlib.md5(pwd.encode("utf8")).hexdigest()
            with models.Session.begin() as sb:
                result = sb.query(models.User).filter_by(nickname=account_name).all()
                if len(result) == 0:
                    self.logintip_label.setText("用户不存在")
                elif result[0].status == UserStatus.LOGOFF:
                    self.logintip_label.setText("该账户已注销，无法使用")
                elif result[0].status == UserStatus.FREEZE:
                    self.logintip_label.setText("该账户已冻结，如需使用，请联系管理员解除冻结")
                elif hex_pwd != result[0].password:
                    self.logintip_label.setText("密码错误")
                else:
                    # 登陆成功存登陆账号密码，添加用户名到系统信息表
                    BaseActivity.logined_user = self.accountname_edit.text()
                    BaseActivity.logined_pwd = self.pwd_edit.text()
                    sysInfo = sb.query(models.SystemInfo).first()
                    if not sysInfo:
                        sb.add(models.SystemInfo())
                    self.saveLoginedInfo()
                    self.timer.stop()
                    self.window.close()
                    self.mainWindowActivity = MainWindowActivity()
                    self.mainWindowActivity.show()
                    self.init_reset_pwd()

    # 设置自动点击
    def onGoAuthLogin(self):
        if self.indicator.isChecked():
            self.login_button.click()

    # 保存登录信息于本地文件用于自动登录
    def saveLoginedInfo(self):
        pwdText = self.pwd_edit.text()
        aes_pwd = encrypt(pwdText)
        settings = QSettings("C:/ProgramData/icarbonx/config/config.ini", QSettings.IniFormat)
        settings.setValue('account', self.accountname_edit.text())
        settings.setValue('password', aes_pwd)
        settings.setValue('authlogin', self.indicator.isChecked())

    # 读取信息
    def initSavedInfo(self):
        settings = QSettings("C:/ProgramData/icarbonx/config/config.ini", QSettings.IniFormat)
        Usr = settings.value('account')
        Pwd_AES = settings.value('password')
        if Pwd_AES:
            Pwd = decrypt(Pwd_AES)
            self.pwd_edit.setText(Pwd)
        ischeck = settings.value('authlogin')
        self.accountname_edit.setText(Usr)
        if ischeck == 'true':
            self.indicator.setChecked(True)
        else:
            self.indicator.setChecked(False)

    def verifyLicense(self):
        flag = licenseUtil.isNeedUpdate()
        if flag:
            self.licenseWidget.setHidden(False)
            self.validLicense = False

    def onVerify(self):
        licenseCode = self.licenseCode_edit.toPlainText()
        if len(licenseCode) == 0:
            self.licenseTip_label.setHidden(False)
        else:
            flag = licenseUtil.isValid(licenseCode)
            if flag:
                licenseUtil.save_license(licenseCode)
                self.licenseWidget.setHidden(True)
                self.validLicense = True
            else:
                self.licenseTip_label.setHidden(False)

    def init_reset_pwd(self):
        current_name = BaseActivity.logined_user
        if current_name:
            with models.Session.begin() as sb:
                user = sb.query(models.User).filter_by(nickname=current_name).first()
                if user.status == UserStatus.INACTIVE:
                    self.resetPwdActivity = ResetPwdActivity()
                    self.resetPwdActivity.exec()

    def eventFilter(self, watched: QObject, event: QEvent):
        if watched == self.closeButton:
            if event.type() == QEvent.HoverEnter:
                self.closeButton.setIcon(QIcon(self.resourcePath("res/ic_white_close.png")))
            elif event.type() == QEvent.HoverLeave:
                self.closeButton.setIcon(QIcon(self.resourcePath("res/ic_del.png")))
        return False


def show():
    try:
        serverName = "AppServer"
        socket = QLocalSocket()
        socket.connectToServer(serverName)
        if socket.waitForConnected(500):
            BaseActivity.app.quit()
        else:
            localServer = QLocalServer()
            localServer.listen(serverName)
            loginActivity = LoginActivity()
            loginActivity.show()
            # loginActivity.verifyLicense()
            sys.exit(BaseActivity.app.exec_())
    except:
        pass


if __name__ == '__main__':
    models.init_db()
    models.init_admin()
    show()
