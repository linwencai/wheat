import sys

from main import models
from main.BaseActivity import BaseActivity
from PySide2.QtCore import Qt
from PySide2.QtGui import QColor, QIcon
from PySide2.QtWidgets import QPushButton, QGraphicsDropShadowEffect, QFrame


class AboutActivity(BaseActivity):
    def __init__(self):
        ui_filename = self.resourcePath("layout/about_layout.ui")
        super().__init__(ui_filename)
        self.window.setWindowModality(Qt.ApplicationModal)
        self.window.setWindowFlags(Qt.FramelessWindowHint)
        self.window.setAttribute(Qt.WA_TranslucentBackground)
        self.close_button: QPushButton = self.window.findChild(QPushButton, 'closeButton')
        self.close_button.setIcon(QIcon(self.resourcePath("res/ic_white_close.png")))
        self.backFrame = self.window.findChild(QFrame, "myFrame")
        self.close_button.clicked.connect(self.close)

        self.setShadow()

    def setShadow(self):
        shadow = QGraphicsDropShadowEffect(self.backFrame)
        shadow.setColor(QColor(0, 0, 0, 80))
        shadow.setOffset(0, 0)
        shadow.setBlurRadius(10)
        self.backFrame.setGraphicsEffect(shadow)

def show():
    aboutActivity = AboutActivity()
    aboutActivity.show()
    sys.exit(BaseActivity.app.exec_())


if "__main__" == __name__:
    models.init_db()
    models.init_admin()
    show()
