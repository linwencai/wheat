from main.BaseActivity import BaseActivity
from main.utils import licenseUtil
from PySide2.QtWidgets import QPushButton, QLabel, QPlainTextEdit, QGraphicsDropShadowEffect, QFrame
from PySide2.QtCore import Qt, QObject, QEvent
from PySide2.QtGui import QColor, QIcon


class EditLicenseActivity(BaseActivity):
    def __init__(self):
        ui_filename = self.resourcePath("layout/license_dialog_layout.ui")
        super().__init__(ui_filename)
        self.window.setWindowModality(Qt.ApplicationModal)
        self.window.setWindowFlags(Qt.FramelessWindowHint)
        self.window.setAttribute(Qt.WA_TranslucentBackground, True)
        self.licenseCode_edit: QPlainTextEdit = self.window.findChild(QPlainTextEdit, "licenseCodeEdit")
        self.licenseTip_label: QLabel = self.window.findChild(QLabel, "licenseTipLabel")
        self.verify_button: QPushButton = self.window.findChild(QPushButton, "verifyButton")
        self.verify_button.clicked.connect(self.onVerify)
        self.closeButton: QPushButton = self.window.findChild(QPushButton, "closeButton")
        self.closeButton.setIcon(QIcon(self.resourcePath("res/ic_del.png")))
        self.closeButton.clicked.connect(self.onClose)
        self.contentFrame: QFrame = self.window.findChild(QFrame, "contentFrame")
        self.licenseTip_label.setHidden(True)
        self.init_license()
        self.setShadow()
        self.closeButton.installEventFilter(self)

    def onVerify(self):
        licenseCode = self.licenseCode_edit.toPlainText()
        if len(licenseCode) == 0:
            self.licenseTip_label.setHidden(False)
        else:
            flag = licenseUtil.isValid(licenseCode)
            if flag:
                licenseUtil.save_license(licenseCode)
                self.close()
            else:
                self.licenseTip_label.setHidden(False)

    def init_license(self):
        currentLicense = licenseUtil.getCurrentLicense()
        self.licenseCode_edit.setPlainText(currentLicense)

    def setShadow(self):
        shadow = QGraphicsDropShadowEffect(self.contentFrame)
        shadow.setColor(QColor(0, 0, 0, 100))
        shadow.setOffset(0, 0)
        shadow.setBlurRadius(10)
        self.contentFrame.setGraphicsEffect(shadow)

    def onClose(self):
        self.close()

    def eventFilter(self, watched: QObject, event: QEvent):
        if watched == self.closeButton:
            if event.type() == QEvent.HoverEnter:
                self.closeButton.setIcon(QIcon(self.resourcePath("res/ic_white_close.png")))
            elif event.type() == QEvent.HoverLeave:
                self.closeButton.setIcon(QIcon(self.resourcePath("res/ic_del.png")))
        return False
