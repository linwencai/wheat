import hashlib
import math
import sys

from PySide2.QtCore import Qt, QTimer
from PySide2.QtGui import QIcon, QIntValidator
from PySide2.QtWidgets import QLineEdit, QPushButton, QLabel, QComboBox, QApplication, QHBoxLayout, QVBoxLayout, \
    QWidget, QAction, QScrollArea, QFrame

from main.AddAccountActivity import AddAccountActivity
from main import models
from main.BaseWidget import BaseWidget
from main.MessageDialog import MessageDialog
from main.utils.Constants import UserRole, UserStatus


class AccountManageActivity(BaseWidget):
    def __init__(self):
        ui_filename = self.resourcePath("layout/account_manage_layout.ui")
        super(AccountManageActivity, self).__init__(ui_filename)
        self._userList = []
        self._pageNum = 1
        self._pageSize = 5
        self._totalPage = 1
        self._totalRecords = 0
        self._userStates = []
        self.addAccountActivity = None
        self._listItemWidget = None
        self._search_name = None

        self.add_account_button = self.window.findChild(QPushButton, "addAccountButton")
        self.add_account_button.clicked.connect(self.onAddAccount)
        self._prev_button = self.window.findChild(QPushButton, "prevButton")
        self._next_button = self.window.findChild(QPushButton, "nextButton")
        self._prev_button.clicked.connect(self.onPrevPage)
        self._next_button.clicked.connect(self.onNextPage)
        self._totalRecord_label = self.window.findChild(QLabel, "totalRecordLabel")
        self._currentPage_label = self.window.findChild(QLabel, "currentPageLabel")
        self._totalPage_label = self.window.findChild(QLabel, "totalPageLabel")
        self._switch_page_edit = self.window.findChild(QLineEdit, "switchPageLineEdit")
        self._switch_page_edit.returnPressed.connect(self.onSwitchPage)
        self._search_edit = self.window.findChild(QLineEdit, "searchEdit")
        self._headerFrame = self.window.findChild(QFrame, "headerFrame")
        self.action = QAction(self)
        self.action.setIcon(QIcon(self.resourcePath("res/search_icon.png")))
        self.action.setText('点击搜索')
        self.action.triggered.connect(self.onSearch)
        self._search_edit.addAction(self.action, QLineEdit.LeadingPosition)
        self._page_record_box = self.window.findChild(QComboBox, "pageRecordBox")
        style = self._page_record_box.styleSheet()
        style = style.replace("../res/ic_down_arrow.png", self.resourcePath("res/ic_down_arrow.png"))
        self._page_record_box.setStyleSheet(style)
        self._scroll_area = self.window.findChild(QScrollArea, "listScrollArea")

        self.init_page_record_box()

        self._onceTimer = QTimer()
        self._onceTimer.timeout.connect(self.onOnce)
        self._onceTimer.start(100)

        self._timer = QTimer()
        self._timer.timeout.connect(self.onTimeout)
        self._timer.start(10000)

    def onOnce(self):
        self.query_total_page()
        self.query_users(self._pageNum, self._pageSize)
        self._onceTimer.stop()
        self._onceTimer = None

    def query_users(self, pageNum, pageSize, realname=None):
        start_index = (pageNum - 1) * pageSize
        query = models.Session().query(models.User)
        userList = query.limit(pageSize).offset(start_index).all()
        if realname:
            userList = query.filter(models.User.realname == realname).limit(pageSize).all()
        self._listItemWidget = AdapterWidget(userList, headerWidget=self._headerFrame)
        self._scroll_area.setWidget(self._listItemWidget)
        self._currentPage_label.setText(str(self._pageNum))
        self._totalPage_label.setText(str(self._totalPage))

    def init_page_record_box(self):
        self._page_record_box.addItems(["5条", "10条", "15条", "20条"])
        self._page_record_box.setCurrentIndex(0)
        self._page_record_box.currentIndexChanged.connect(self.onPageRecordChanged)

    def query_total_page(self):
        self._totalRecords = models.Session().query(models.User).count()
        self._totalPage = math.ceil(self._totalRecords / self._pageSize)
        self._totalRecord_label.setText(str(self._totalRecords))
        if self._totalRecords == 0:
            self._currentPage_label.setText("0")
            self._totalPage_label.setText("0")
            self._switch_page_edit.setValidator(QIntValidator(0, 0))
        else:
            self._currentPage_label.setText(str(self._pageNum))
            self._totalPage_label.setText(str(self._totalPage))
            self._switch_page_edit.setValidator(QIntValidator(1, self._totalPage))

    def onPageRecordChanged(self):
        index = self._page_record_box.currentIndex()
        if index == 0:
            self._pageSize = 5
        elif index == 1:
            self._pageSize = 10
        elif index == 2:
            self._pageSize = 15
        else:
            self._pageSize = 20
        self._totalPage = math.ceil(self._totalRecords / self._pageSize)
        if self._pageNum > self._totalPage:
            self._pageNum = self._totalPage
        self._currentPage_label.setText(str(self._pageNum))
        self._totalPage_label.setText(str(self._totalPage))
        self._switch_page_edit.setValidator(QIntValidator(1, self._totalPage))
        self.query_users(self._pageNum, self._pageSize)

    def onTimeout(self):
        self.query_users(self._pageNum, self._pageSize, self._search_name)

    def onPrevPage(self):
        if self._pageNum <= 1:
            self._pageNum = 1
            return
        self._pageNum = self._pageNum - 1
        self.query_users(self._pageNum, self._pageSize, self._search_name)

    def onNextPage(self):
        if self._pageNum >= self._totalPage:
            self._pageNum = self._totalPage
            return
        self._pageNum = self._pageNum + 1
        self.query_users(self._pageNum, self._pageSize, self._search_name)

    def onSwitchPage(self):
        pageNum = int(self._switch_page_edit.text())
        if pageNum == self._pageNum or pageNum < 1 or pageNum > self._totalPage:
            return
        self._pageNum = pageNum
        self.query_users(self._pageNum, self._pageSize, self._search_name)
        self._switch_page_edit.clear()

    def onSearch(self):
        self._search_name = self._search_edit.text()
        self.query_users(self._pageNum, self._pageSize, self._search_name)
        # self._search_edit.clear()

    def onAddAccount(self):
        self._search_name = None
        self._search_edit.clear()
        win_x = self.topLevelWidget().pos().x()
        win_y = self.topLevelWidget().pos().y()
        self.addAccountActivity = AddAccountActivity()
        self.addAccountActivity.window.move(win_x+185, win_y+107)
        self.addAccountActivity.signal.update_signal.connect(self.add_account)
        self.addAccountActivity.show()
    """
    当新增账户后，会调用该方法
    """
    def add_account(self, flag: bool):
        self.query_total_page()
        self.query_users(self._pageNum, self._pageSize, self._search_name)


class AdapterWidget(QWidget):
    def __init__(self, userList,  headerWidget):
        super(AdapterWidget, self).__init__()
        self._userList = userList
        self._headerWidget = headerWidget
        self._itemList = []
        self.vertical_layout = QVBoxLayout(self)
        self.vertical_layout.setSpacing(0)
        self.vertical_layout.setMargin(0)
        self.vertical_layout.setAlignment(Qt.AlignTop)
        self.get_item()

    def get_item(self):
        userNum = len(self._userList)
        itemNum = len(self._itemList)
        count = userNum if userNum >= itemNum else itemNum
        for i in range(count):
            if userNum >= itemNum:
                user = self._userList[i]
                if i < itemNum:
                    item = self._itemList[i]
                else:
                    item = ListItemWidget(headerWidget=self._headerWidget)
                    self._itemList.append(item)
                    item.setFixedHeight(34)
                    self.vertical_layout.addWidget(item)
                self.setListItem(item, user)
            else:
                if i < userNum:
                    user = self._userList[i]
                    item = self._itemList[i]
                    self.setListItem(item, user)
                else:
                    item = self._itemList[-1]
                    self.vertical_layout.removeWidget(item)
                    self._itemList.remove(item)
                    item.close()

    def setListItem(self, item, user):
        item.id_label.setText(str(user.id))
        item.nickname_label.setText(user.nickname)
        item.realname_label.setText(user.realname)
        item.phone_label.setText(user.phoneNumber)
        if user.role == UserRole.ADMINISTRATOR:
            user.role = "管理员"
        elif user.role == UserRole.GENERAL:
            user.role = "普通用户"
        update_at = str(user.update_at).split(".")[0].replace("-", "")
        item.role_label.setText(user.role)
        item.status_label.setText(user.status)
        item.update_at_label.setText(update_at)
        item.isFreezeButton.tag = user.id
        item.deleteButton.tag = user.id
        item.resetButton.tag = user.id
        status = user.status
        if status == UserStatus.ACTIVE or status == UserStatus.INACTIVE:
            item.isFreezeButton.setText("冻结")
        elif status == UserStatus.FREEZE:
            item.isFreezeButton.setText("解冻")
        elif status == UserStatus.LOGOFF:
            item.isFreezeButton.setVisible(False)
            item.deleteButton.setVisible(False)
            item.resetButton.setVisible(False)
        else:
            item.isFreezeButton.setText("冻结")


class MyPushButton(QPushButton):
    def __init__(self, text):
        super(MyPushButton, self).__init__(text=text)
        self.tag = None


class ListItemWidget(QWidget):
    def __init__(self, headerWidget):
        super(ListItemWidget, self).__init__()
        self.headerWidget = headerWidget
        self.init_ui()

    def init_ui(self):
        heri_layout1 = QHBoxLayout()
        heri_layout1.setAlignment(Qt.AlignCenter)
        heri_layout1.setSpacing(0)
        heri_layout1.setMargin(0)
        self.id_label = QLabel("01")
        self.id_label.setFixedWidth(self.headerWidget.findChild(QLabel, "idLabel").width())
        self.id_label.setStyleSheet("font-size: 10px;color: #4A4A4A;")
        heri_layout1.addWidget(self.id_label, stretch=13)
        self.nickname_label = QLabel("admin")
        self.nickname_label.setFixedWidth(self.headerWidget.findChild(QLabel, "accountLabel").width())
        self.nickname_label.setStyleSheet("font-size: 10px;color: #4A4A4A;")
        heri_layout1.addWidget(self.nickname_label, stretch=13)
        self.realname_label = QLabel("碳小云")
        self.realname_label.setFixedWidth(self.headerWidget.findChild(QLabel, "realnameLabel").width())
        self.realname_label.setStyleSheet("font-size: 10px;color: #4A4A4A;")
        heri_layout1.addWidget(self.realname_label, stretch=12)
        self.phone_label = QLabel("18600009999")
        self.phone_label.setFixedWidth(self.headerWidget.findChild(QLabel, "phoneLabel").width())
        self.phone_label.setStyleSheet("font-size: 10px;color: #4A4A4A;")
        heri_layout1.addWidget(self.phone_label, stretch=15)
        self.role_label = QLabel("管理员")
        self.role_label.setFixedWidth(self.headerWidget.findChild(QLabel, "roleLabel").width())
        self.role_label.setStyleSheet("font-size: 10px;color: #4A4A4A;")
        heri_layout1.addWidget(self.role_label, stretch=12)
        self.status_label = QLabel("使用中")
        self.status_label.setFixedWidth(self.headerWidget.findChild(QLabel, "statusLabel").width())
        self.status_label.setStyleSheet("font-size: 10px;color: #4A4A4A;")
        heri_layout1.addWidget(self.status_label, stretch=12)
        self.update_at_label = QLabel("20210501 17:00:01")
        self.update_at_label.setFixedWidth(self.headerWidget.findChild(QLabel, "updateLabel").width())
        self.update_at_label.setStyleSheet("font-size: 10px;color: #4A4A4A;")
        heri_layout1.addWidget(self.update_at_label, stretch=24)

        heri_layout2 = QHBoxLayout()
        heri_layout2.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        heri_layout2.setSpacing(0)
        heri_layout2.setMargin(0)
        self.isFreezeButton = MyPushButton("冻结")
        self.isFreezeButton.setStyleSheet("QPushButton{font-size:10px;color:#4A90E2;border:none;} "
                                          "QPushButton:hover{color:rgba(74,144,226,0.6);}")
        self.isFreezeButton.clicked.connect(self._onIsFreeze)
        heri_layout2.addWidget(self.isFreezeButton)
        self.deleteButton = MyPushButton("注销")
        self.deleteButton.setStyleSheet("QPushButton{font-size:10px;color:#F44A6C;border:none;} "
                                        "QPushButton:hover{color:rgba(244,74,108,0.6);}")
        self.deleteButton.clicked.connect(self._onDelete)
        heri_layout2.addWidget(self.deleteButton)
        self.resetButton = MyPushButton("重设密码")
        self.resetButton.setStyleSheet("QPushButton{font-size:10px;color:#4A90E2;border:none;} "
                                       "QPushButton:hover{color:rgba(74,144,226,0.6);}")
        self.resetButton.clicked.connect(self._onReset)
        heri_layout2.addWidget(self.resetButton)
        heri_layout2.setSpacing(10)
        operationWidget = QWidget()
        operationWidget.setLayout(heri_layout2)
        operationWidget.setFixedWidth(self.headerWidget.findChild(QLabel, "operationLabel").width())

        separate_label = QLabel()
        separate_label.setFixedHeight(1)
        separate_label.setStyleSheet("background-color:  #EAEAEC;")

        heri_layout = QHBoxLayout()
        heri_layout.setSpacing(0)
        heri_layout.setMargin(0)
        heri_layout.addLayout(heri_layout1)
        heri_layout.addWidget(operationWidget)

        content_layout = QVBoxLayout()
        content_layout.setSpacing(0)
        content_layout.setMargin(0)
        content_layout.addLayout(heri_layout)
        content_layout.addWidget(separate_label)

        self.window().setLayout(content_layout)

    def _onIsFreeze(self):
        self.is_freeze_pushbutton = self.sender()
        if self.is_freeze_pushbutton.text() == "冻结":
            with models.Session.begin() as sb:
                user = sb.query(models.User).filter_by(id=self.is_freeze_pushbutton.tag).first()
            if user.status == UserStatus.ACTIVE:
                msg_box = MessageDialog()
                msg_box.signal.update_signal.connect(self._freezeAccount)
                msg_box.setTitle("提示")
                msg_box.setMsg("冻结后，该账户将无法使用（如需继续使用需解除冻结）是否继续？")
                msg_box.setBtn("继续")
                msg_box.exec()
            else:
                msg_box = MessageDialog()
                msg_box.setTitle("错误提示")
                msg_box.setMsg("仅可对状态为“使用中”的账户，进行冻结账户！")
                msg_box.exec()

        elif self.is_freeze_pushbutton.text() == "解冻":
            with models.Session.begin() as sb:
                user = sb.query(models.User).filter_by(id=self.is_freeze_pushbutton.tag).first()
            if user.status == UserStatus.FREEZE:
                msg_box = MessageDialog()
                msg_box.signal.update_signal.connect(self._unfreezeAccount)
                msg_box.setTitle("提示")
                msg_box.setMsg("解除冻结后，该账户将解除冻结状态，恢复正常使用。是否继续？")
                msg_box.setBtn("继续")
                msg_box.exec()
            else:
                msg_box = MessageDialog()
                msg_box.setTitle("错误提示")
                msg_box.setMsg("仅可对状态为“已冻结”的账户，进行解除冻结！")
                msg_box.exec()

    def _onDelete(self):
        self.del_pushbutton = self.sender()
        with models.Session.begin() as sb:
            user = sb.query(models.User).filter_by(id=self.del_pushbutton.tag).first()
        if user.status == UserStatus.ACTIVE or user.status == UserStatus.FREEZE or user.status == UserStatus.INACTIVE:
            msg_box = MessageDialog()
            msg_box.signal.update_signal.connect(self._deleteAccount)
            msg_box.setTitle("提示")
            msg_box.setMsg("注销后，该账户将永久无法使用。是否继续？")
            msg_box.setBtn("继续")
            msg_box.exec()
        else:
            msg_box = MessageDialog()
            msg_box.setTitle("错误提示")
            msg_box.setMsg("仅可对状态为“待激活、使用中、已冻结”的账户，进行注销！")
            msg_box.exec()

    def _onReset(self):
        self.reset_pushbutton = self.sender()
        with models.Session.begin() as sb:
            user = sb.query(models.User).filter_by(id=self.reset_pushbutton.tag).first()
        if user.status == UserStatus.ACTIVE or user.status == UserStatus.FREEZE:
            msg_box = MessageDialog()
            msg_box.signal.update_signal.connect(self._resetPassword)
            msg_box.setTitle("提示")
            msg_box.setMsg("重置密码后，该账户密码将恢复为初始密码。是否继续？")
            msg_box.setBtn("继续")
            msg_box.exec()
        else:
            msg_box = MessageDialog()
            msg_box.setTitle("错误提示")
            msg_box.setMsg("仅可对状态为“使用中、已冻结”的账户，进行重置密码！")
            msg_box.exec()

    def _freezeAccount(self):
        with models.Session.begin() as sb:
            user = sb.query(models.User).filter_by(id=self.is_freeze_pushbutton.tag).first()
            user.status = UserStatus.FREEZE
        msg_box = MessageDialog()
        msg_box.setTitle("成功提示")
        msg_box.setMsg("%s账户，已经冻结。" % user.nickname)
        msg_box.exec()

    def _unfreezeAccount(self):
        with models.Session.begin() as sb:
            user = sb.query(models.User).filter_by(id=self.is_freeze_pushbutton.tag).first()
            user.status = UserStatus.ACTIVE
        msg_box = MessageDialog()
        msg_box.setTitle("成功提示")
        msg_box.setMsg("%s账户，已经解除冻结。" % user.nickname)
        msg_box.exec()

    def _deleteAccount(self):
        with models.Session.begin() as sb:
            user = sb.query(models.User).filter_by(id=self.del_pushbutton.tag).first()
            user.status = UserStatus.LOGOFF
        msg_box = MessageDialog()
        msg_box.setTitle("成功提示")
        msg_box.setMsg("%s账户，已经注销。" % user.nickname)
        msg_box.exec()

    def _resetPassword(self):
        with models.Session.begin() as sb:
            user = sb.query(models.User).filter_by(id=self.reset_pushbutton.tag).first()
            init_pwd = hashlib.md5((user.nickname + user.phoneNumber[-4:]).encode("utf8")).hexdigest()
            user.password = init_pwd
        msg_box = MessageDialog()
        msg_box.setTitle("成功提示")
        msg_box.setMsg("%s账户，已经重置密码。" % user.nickname)
        msg_box.exec()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    accountManage = AccountManageActivity()
    accountManage.show()
    sys.exit(app.exec_())




