import logging

LOG_FMT = '%(asctime)s | %(levelname)s | %(message)s'
DATEFMT = "%Y%m%d %H:%M:%S"


class Logger(object):
    def __init__(self, log_file_path):
        self._log = logging.getLogger("task_logger")
        self._log.setLevel(logging.DEBUG)
        log_handler = logging.FileHandler(filename=log_file_path)
        logfile_formatter = logging.Formatter(
            LOG_FMT,
            datefmt=DATEFMT)
        log_handler.setFormatter(logfile_formatter)
        self._log.addHandler(log_handler)

    def debug_to_file(self, message):
        self._log.debug(message)
