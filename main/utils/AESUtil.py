from binascii import b2a_hex, a2b_hex

import pyaes


def adapterText(pwdText):
    """
    密码数字不足用空格补齐
    :param pwdText: str
    :return: text:byte
    """
    textCount = len(pwdText)
    if (textCount % 16) != 0:
        addNum = 16 - textCount % 16
    else:
        addNum = 0

    text = pwdText + ('\0' * addNum)

    return text.encode("utf-8")

key = "iCarbonx@2021712".encode('utf-8')
iv = "iCarbonx@2021712".encode('utf-8')


def encrypt(pwdText):
    aes = pyaes.AESModeOfOperationOFB(key=key, iv=iv)
    aesPwd = aes.encrypt(pwdText)
    return b2a_hex(aesPwd).decode(encoding="utf8")


def decrypt(pwd_AES):
    aes = pyaes.AESModeOfOperationOFB(key=key, iv=iv)
    pwd_Byte = aes.decrypt(a2b_hex(pwd_AES))
    return pwd_Byte.decode(encoding="utf8")
if __name__ == '__main__':

    e = encrypt("Aa12345612345678ghjhg")
    print(type(e))
    d = decrypt(e)
    print(type(d))
    print("加密：{}，解密{}".format(e,d))