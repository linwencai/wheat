import rsa
import base64
import datetime
import json
from main import models
from main.utils import privateKey

def isNeedUpdate():
    licenseEntity = models.Session().query(models.LicenseEntity).first()
    if not licenseEntity:
        return True
    else:
        oldCode = licenseEntity.licenseCode
        valid = isValid(oldCode)
        return not valid

def isValid(licenseCode):
    try:
        priv_key = rsa.PrivateKey.load_pkcs1(privateKey.key.encode("utf8"))
        original_data = rsa.decrypt(base64.b64decode(licenseCode, validate=True), priv_key)
        licenseDic = json.loads(original_data.decode("utf8").replace("'", "\""))
        expire_time = licenseDic.get("expire", 1)
        checkItems = licenseDic.get("checkItems", -1)
        now_time = int(datetime.datetime.now().timestamp())
        if now_time > expire_time:
            return False
        elif checkItems <= 0 or checkItems > 3:
            return False
        else:
            return True
    except Exception as ep:
        print(ep)
        return False

def save_license(licenseCode):
    with models.Session.begin() as sb:
        entity = sb.query(models.LicenseEntity).first()
        if entity:
            entity.licenseCode = licenseCode
        else:
            sb.add(models.LicenseEntity(licenseCode=licenseCode))

def getCurrentLicense():
    licenseEntity = models.Session().query(models.LicenseEntity).first()
    if not licenseEntity:
        return ""
    else:
        oldCode = licenseEntity.licenseCode
        return oldCode

