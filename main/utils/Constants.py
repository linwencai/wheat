class State(object):
    #闲置
    IDLE = "Idle"
    #排队中
    QUEUING = "Queuing"
    #运行中,提取数据
    EXTRACTING_DATA = "extracting_data"
    #运行中，分析数据
    ANALYSING_DATA = "analysing_data"
    #任务完成
    COMPLETE = "Complete"
    #任务失败
    FAILURE = "Failure"
    #任务取消
    CANCEL = "Cancel"


class UserRole(object):
    #管理员
    ADMINISTRATOR = 0
    #普通用户
    GENERAL = 1


class ChipVer(object):
    V13 = "V13"
    V16 = "V16"


class InspectionItem(object):
    SARCOIDOSIS = "肺结节良恶性分析"
    DM = "糖尿病分析"


class UserStatus(object):
    INACTIVE = "待激活"
    ACTIVE = "使用中"
    FREEZE = "已冻结"
    LOGOFF = "已注销"

