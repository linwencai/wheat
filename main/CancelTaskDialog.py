import sys

from PySide2.QtCore import Qt, QObject, Signal
from PySide2.QtGui import QPixmap, QColor, QPalette, QBrush
from PySide2.QtWidgets import QPushButton, QLabel, QGraphicsDropShadowEffect, QWidget, QGridLayout

from main.BaseActivity import BaseActivity

class MySignal(QObject):
    Signal = Signal(bool)

class CancelTips(BaseActivity):
    def __init__(self):
        ui_filename = self.resourcePath("layout/cancel_tip_layout.ui")
        super(CancelTips, self).__init__(ui_layout=ui_filename)
        self.btnYes = self.window.findChild(QPushButton, 'yes_btn')
        self.btnNo = self.window.findChild(QPushButton, 'no_btn')
        self.ic_label: QLabel = self.window.findChild(QLabel, 'ic_label')
        self.baseWidget:QWidget = self.window.findChild(QWidget,'baseWidget')
        self.myWidget:QWidget = self.window.findChild(QWidget,'myWidget')

        self.btnYes.clicked.connect(self.onYes)
        self.btnNo.clicked.connect(self.onNo)
        self.mysignal =MySignal()

        self.initUI()
        self.addShadow()

    def initUI(self):
        self.ic_label.setPixmap(QPixmap(self.resourcePath('res/cancel_icon')))
        self.ic_label.setScaledContents(True)
        self.window.setWindowFlags(Qt.FramelessWindowHint)
        self.window.setAttribute(Qt.WA_TranslucentBackground)
        palette = self.myWidget.palette()
        palette.setBrush(QPalette.Window, QBrush(QPixmap(self.resourcePath("res/ic_dialog_bg.svg")).scaled(self.myWidget.size(),Qt.IgnoreAspectRatio,Qt.SmoothTransformation)))
        self.myWidget.setPalette(palette)
        self.baseLayout = QGridLayout()
        self.baseLayout.setSpacing(0)
        self.baseLayout.setMargin(4)
        self.baseLayout.addWidget(self.myWidget)
        self.baseWidget.setLayout(self.baseLayout)
        self.baseWidget.setAttribute(Qt.WA_TranslucentBackground)

    def addShadow(self):
        shadow = QGraphicsDropShadowEffect(self.myWidget)
        shadow.setOffset(0, 0)
        shadow.setBlurRadius(10)
        shadow.setColor(QColor(200, 200, 200))
        self.myWidget.setGraphicsEffect(shadow)

    def onYes(self):
        self.mysignal.Signal.emit(True)

    def onNo(self):
        self.window.close()

if __name__ == '__main__':
    a = CancelTips()
    a.show()
    sys.exit(BaseActivity.app.exec_())

