import ctypes
import sys
from PySide2.QtWidgets import QApplication
from PySide2.QtCore import QFile, QIODevice, QObject, Qt
from PySide2.QtUiTools import QUiLoader
from PySide2.QtGui import QIcon, QFont


class BaseActivity(QObject):
    app = None
    # 保存登陆用户名密码
    logined_user = None
    logined_pwd = None

    def __init__(self, ui_layout: str):
        super(BaseActivity, self).__init__()
        self.window = None
        if not BaseActivity.app:
            QApplication.setAttribute(Qt.AA_EnableHighDpiScaling)
            # 取消程序自适应屏幕大小时的按四舍五入放大这策略
            QApplication.setHighDpiScaleFactorRoundingPolicy(Qt.HighDpiScaleFactorRoundingPolicy.PassThrough)
            QApplication.setFont(QFont('Microsoft YaHei',9, QFont.Normal))
            QApplication.setAttribute(Qt.AA_UseHighDpiPixmaps)
            BaseActivity.app = QApplication(sys.argv)
        if ui_layout:
            q_file = QFile(ui_layout)
            q_file.open(QIODevice.ReadOnly)
            ui_loader = QUiLoader()
            self.window = ui_loader.load(q_file)
            q_file.close()
        self.window.setWindowFlag(Qt.WindowContextHelpButtonHint, False)
        self.window.setWindowFlag(Qt.WindowMaximizeButtonHint, False)
        self.window.setWindowIcon(QIcon(self.resourcePath("res/ic_logo.jpg")))

        ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID("myappid")

    def show(self):
        if BaseActivity.app and self.window:
            self.window.show()

    def exec(self):
        if BaseActivity.app and self.window:
            self.window.exec()

    def close(self):
        if self.window:
            self.window.close()

    @staticmethod
    def resourcePath(relative_path):
        """ Get absolute path to resource, works for dev and for PyInstaller """
        base_path = getattr(sys, '_MEIPASS', '..')
        # stylesheet中的url只能用 /
        base_path = base_path.replace("\\", "/")
        return base_path + "/" + relative_path


