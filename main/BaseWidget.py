import sys

from PySide2.QtWidgets import QVBoxLayout, QWidget
from PySide2.QtCore import QFile, QIODevice
from PySide2.QtUiTools import QUiLoader

class BaseWidget(QWidget):
    def __init__(self, ui_layout: str):
        super(BaseWidget, self).__init__()
        self.window = None
        if ui_layout:
            q_file = QFile(ui_layout)
            q_file.open(QIODevice.ReadOnly)
            ui_loader = QUiLoader()
            self.window = ui_loader.load(q_file)
            q_file.close()

        self.vboxLayout = QVBoxLayout()
        self.vboxLayout.addWidget(self.window)
        self.vboxLayout.setContentsMargins(0, 0, 0, 0)
        self.vboxLayout.setSpacing(0)
        self.vboxLayout.setMargin(0)
        self.setLayout(self.vboxLayout)


    @staticmethod
    def resourcePath(relative_path):
        """ Get absolute path to resource, works for dev and for PyInstaller """
        base_path = getattr(sys, '_MEIPASS', '..')
        # stylesheet中的url只能用 /
        base_path = base_path.replace("\\", "/")
        return base_path + "/" + relative_path
