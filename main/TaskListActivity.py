import os
import subprocess
import sys
import math
import datetime
from main.BaseWidget import BaseWidget
from main import models
from main.utils import Constants
from main.task.TaskManager import TaskManager
from main.AnalysisLogActivity import AnalysisLogActivity
from main.CancelTaskDialog import CancelTips
from PySide2.QtWidgets import QWidget, QLabel, QPushButton, \
    QMenu, QVBoxLayout, QHBoxLayout, QScrollArea, QToolButton, \
    QFrame, QDateEdit, QLineEdit, QGraphicsDropShadowEffect, QComboBox, QMessageBox, QApplication, QAction
from PySide2.QtCore import Qt, QDate, QTimer, QPoint, QEvent, QObject, QUrl
from PySide2.QtGui import QIntValidator, QIcon, QColor, QImage, qRgb, QDesktopServices


class TaskListActivity(BaseWidget):
    def __init__(self, status: list = None):
        ui_filename = self.resourcePath("layout/task_list_layout.ui")

        super(TaskListActivity, self).__init__(ui_layout=ui_filename)
        self.status = status
        self._taskList = []
        self._pageNum = 1
        self._pageSize = 5
        self._totalPage = 1
        self._totalRecords = 0
        self._taskStates = []
        self._selectedTaskStates = []
        self._selectedOperators = []
        self._selectedTiffName = None
        self._selectedBatch = None
        self.creator_button: QToolButton = self.window.findChild(QToolButton, "creator_button")
        self.batch_button: QToolButton = self.window.findChild(QToolButton, "batch_button")
        self._totalRecord_label: QLabel = self.window.findChild(QLabel, "totalRecordLabel")
        self._currentPage_label: QLabel = self.window.findChild(QLabel, "currentPageLabel")
        self._pre_button: QPushButton = self.window.findChild(QPushButton, "preButton")
        self._next_button: QPushButton = self.window.findChild(QPushButton, "nextButton")
        self._pre_button.clicked.connect(self.onPrePage)
        self._next_button.clicked.connect(self.onNextPage)
        self.skipButton:QPushButton = self.window.findChild(QPushButton,'skipButton')
        self.skipButton.clicked.connect(self.onForwardToPage)
        self._pageNum_edit: QLineEdit = self.window.findChild(QLineEdit, "pageNumEdit")
        self._pageNum_edit.returnPressed.connect(self.onForwardToPage)
        self._startDate = None
        self._endDate = None
        self._search_edit: QLineEdit = self.window.findChild(QLineEdit, "searchEdit")
        self._search_edit.returnPressed.connect(self.onSearch)
        self._top_frame:QFrame = self.window.findChild(QFrame,'Top_frame')
        self._bottom_frame: QFrame = self.window.findChild(QFrame,'Bottom_frame')
        self._top_frame.setAttribute(Qt.WA_TranslucentBackground,True)
        self._headerFrame: QFrame = self.window.findChild(QFrame, "headerFrame")
        self._titleLabel: QLabel = self.window.findChild(QLabel, "titleLabel")
        _act = QAction(self)
        _act.setIcon(QIcon(self.resourcePath("res/search_icon.png")))
        _act.setText('点击搜索')
        _act.triggered.connect(self.onSearch)
        self._search_edit.addAction(_act,QLineEdit.LeadingPosition)
        self._pageSize_box: QComboBox = self.window.findChild(QComboBox, "pageSizeBox")
        style = self._pageSize_box.styleSheet()
        style = style.replace("../res/ic_down_arrow.png", self.resourcePath("res/ic_down_arrow.png"))
        self._pageSize_box.setStyleSheet(style)
        self._scroll_area: QScrollArea = self.window.findChild(QScrollArea, "listScrollArea")
        self._scroll_area.viewport().setStyleSheet("background:transparent;")  #设置视口背景透明
        #
        # from main.MainWindowActivity import MainWindowActivity
        # self.mainwindow= MainWindowActivity()
        # self.mainwindow._batchsignal.hideSignal.connect(self.onHideBatch)


        self.init_task_states()
        self.init_menu_button()
        self.init_pageSize_box()

        if status:
            self._selectedTaskStates.extend(status)

        self._onceTimer = QTimer()
        self._onceTimer.timeout.connect(self.onOnce)
        self._onceTimer.start(300)

        self._timer = QTimer()
        self._timer.timeout.connect(self.onTimeout)
        self._timer.start(10000)

    def onOnce(self):
        self.query_tasks(self._pageNum, self._pageSize)
        self._onceTimer.stop()
        self._onceTimer = None

    #1查询工具人,初始页
    def query_tasks(self, pageNum, pageSize, task_states = None, operators = None, batch = None, tiff_name = None):
        start_index = (pageNum - 1) * pageSize
        query = models.Session().query(models.Tasks)
        if task_states and len(task_states) > 0:
            query = query.filter(models.Tasks.state.in_(task_states))
        if operators and len(operators) > 0:
            query = query.filter(models.Tasks.operator.in_(operators))
        if batch and len(batch) == 2:
            query = query.filter(models.Tasks.createTime >= batch[0], models.Tasks.createTime <= batch[1])
        if tiff_name and len(tiff_name) > 0:
            query = query.filter(models.Tasks.tiffFile.like("%"+tiff_name))
        totalRecords = query.count()
        self.count_total_page(totalRecords)
        taskList = query.order_by(models.Tasks.createTime.desc()).limit(pageSize).offset(start_index).all()
        self._listItemWidget = AdapterWidget(taskList, headerWidget=self._headerFrame) # 查询任务时候重新适配scrollarea的内容，解决Item重叠bug
        self._scroll_area.setWidget(self._listItemWidget)
        # self._listItemWidget.refresh(taskList)

    # 设置总个数，当前页/总页
    def count_total_page(self, totalRecords):
        self._totalRecords = totalRecords
        self._totalPage = math.ceil(self._totalRecords / self._pageSize) #返回大于或小于的最小整数
        self._totalRecord_label.setText(str(self._totalRecords))
        if self._totalRecords == 0:
            self._currentPage_label.setText("0/0")
            self._pageNum_edit.setValidator(QIntValidator(0, 0))
        else:
            self._currentPage_label.setText("{0}/{1}".format(self._pageNum, self._totalPage))
            self._pageNum_edit.setValidator(QIntValidator(1, self._totalPage))

    def init_task_states(self):
        self._taskStates.append({"text":"请求分析", "states": [Constants.State.IDLE, Constants.State.QUEUING]})
        self._taskStates.append(
            {"text": "数据提取", "states": [Constants.State.EXTRACTING_DATA]})
        self._taskStates.append(
            {"text": "校验分析", "states": [Constants.State.ANALYSING_DATA]})
        self._taskStates.append(
            {"text": "完成", "states": [Constants.State.COMPLETE]})
        self._taskStates.append(
            {"text": "已取消", "states": [Constants.State.CANCEL]})
        self._taskStates.append(
            {"text": "异常", "states": [Constants.State.FAILURE]})

    def init_menu_button(self):

        self.creator_menu = QMenu(self.creator_button)
        self.creator_menu.setStyleSheet("QMenu::item:selected {"
                                        "border-color: darkblue;"
                                        "background: rgba(200, 200, 200, 150);}"
                                        "QMenu{background-color:#FFFFFF}"
                                        )
        self.creator_button.setMenu(self.creator_menu)
        self.creator_button.setPopupMode(QToolButton.InstantPopup)
        # 添加创建人筛选按钮信号
        userList = models.Session().query(models.User).all()
        for user in userList:
            self.creator_menu.addAction(user.nickname, self.onOperator).setCheckable(True)
        # 添加校验批次的筛选项信号
        self.sty = self.batch_button.styleSheet()
        self.window.installEventFilter(self)
        self.dateEdit_frame: QFrame = self.window.findChild(QFrame, 'dateEdit_frame')
        self.setShadow()
        self.startDate: QDateEdit = self.window.findChild(QDateEdit, 'dateEdit')
        self.endDate: QDateEdit = self.window.findChild(QDateEdit, 'dateEdit_2')
        self.cancelDateBtn:QToolButton = self.window.findChild(QToolButton, 'cancelBatch')
        self.dateEdit_frame.setVisible(False)

        self.startDate.setCalendarPopup(True)
        self.startDate.setDisplayFormat('yyyyMMdd')
        self.startDate.lineEdit().setReadOnly(True)
        self.startDate.installEventFilter(self) #禁止滚轮改变数字

        self.endDate.setCalendarPopup(True)
        self.endDate.setDisplayFormat('yyyyMMdd')
        self.endDate.lineEdit().setReadOnly(True)
        self.endDate.installEventFilter(self)

        self.currentDate = QDate.currentDate()
        self.startDate.setDate(self.currentDate)
        self.endDate.setDate(self.currentDate)

        self._endDate = self.endDate.date()
        self._startDate = self.startDate.date()
        self.startDate.setMaximumDate(self._endDate)
        self.endDate.setMinimumDate(self._startDate)

        startStyle = self.startDate.styleSheet()
        startStyle = startStyle.replace("../res/ic_down_arrow.png", self.resourcePath("res/ic_down_arrow.png"))
        self.startDate.setStyleSheet(startStyle)
        endStyle = self.endDate.styleSheet()
        endStyle = endStyle.replace("../res/ic_down_arrow.png", self.resourcePath("res/ic_down_arrow.png"))
        self.endDate.setStyleSheet(endStyle)

        self.startDate.calendarWidget().setStyleSheet('background:none')
        self.endDate.calendarWidget().setStyleSheet('background:none')

        self.batch_button.clicked.connect(self.onShowDataEditFrame)
        self.endDate.dateChanged.connect(self.onDateChanged)
        self.startDate.dateChanged.connect(self.onDateChanged)
        self.cancelDateBtn.clicked.connect(self.onRecover)
    # 批次筛选
    def onDateChanged(self):
        # 限制时间可选范围
        startdate = self.startDate.date()
        enddate = self.endDate.date()

        self.endDate.setMinimumDate(startdate)
        self.startDate.setMaximumDate(enddate)

        startTime = int(datetime.datetime(startdate.year(), startdate.month(), startdate.day(), 0, 0, 0).timestamp())
        endTime = int(datetime.datetime(enddate.year(), enddate.month(), enddate.day(), 23, 59, 59).timestamp())
        self._selectedBatch = (startTime, endTime)
        self._pageNum = 1
        self.query_tasks(self._pageNum, self._pageSize,
                         task_states=self._selectedTaskStates,
                         operators=self._selectedOperators,
                         batch=self._selectedBatch,
                         tiff_name=self._selectedTiffName)


    def onRecover(self):
        self.batch_button.setStyleSheet(self.sty)
        self.endDate.setDate(self.currentDate)
        self.startDate.setDate(self.currentDate)

        self._selectedBatch = None   #重置防止取消后 10秒刷新再读当前前筛选日期
        self._pageNum = 1
        self.query_tasks(self._pageNum, self._pageSize,
                         task_states=self._selectedTaskStates,
                         operators=self._selectedOperators,
                         tiff_name=self._selectedTiffName)

        self.dateEdit_frame.setHidden(True)

    def setShadow(self):
        shadow = QGraphicsDropShadowEffect(self.dateEdit_frame)
        shadow.setColor(QColor(200,200,200))
        shadow.setXOffset(4.0)
        shadow.setYOffset(4.0)
        shadow.setBlurRadius(8)
        self.dateEdit_frame.setGraphicsEffect(shadow)

    def init_pageSize_box(self):
        self._pageSize_box.addItems(["5条/页", "10条/页", "15条/页", "20条/页"])
        self._pageSize_box.setCurrentIndex(0)
        self._pageSize_box.currentIndexChanged.connect(self.onPageSizeChanged)

    def onPageSizeChanged(self):
        index = self._pageSize_box.currentIndex()
        if index == 0:
            self._pageSize = 5
        elif index == 1:
            self._pageSize = 10
        elif index == 2:
            self._pageSize = 15
        else:
            self._pageSize = 20
        self._totalPage = math.ceil(self._totalRecords / self._pageSize)
        if self._pageNum > self._totalPage:
            self._pageNum = self._totalPage
        self._currentPage_label.setText("{0}/{1}".format(self._pageNum, self._totalPage))
        self._pageNum_edit.setValidator(QIntValidator(1, self._totalPage))
        self.query_tasks(self._pageNum, self._pageSize,
                         task_states=self._selectedTaskStates,
                         operators=self._selectedOperators,
                         batch=self._selectedBatch,
                         tiff_name=self._selectedTiffName)

    def onProgress(self):
        self._selectedTaskStates.clear()
        for action in self.progress_menu.actions():
            for task_state in self._taskStates:
                if action.text() == task_state["text"] and action.isChecked():
                     self._selectedTaskStates = self._selectedTaskStates + task_state["states"]
        self._pageNum = 1
        self.query_tasks(self._pageNum, self._pageSize,
                         task_states=self._selectedTaskStates,
                         operators=self._selectedOperators,
                         batch=self._selectedBatch,
                         tiff_name=self._selectedTiffName)

    def onOperator(self):
        self._selectedOperators.clear()
        for action in self.creator_menu.actions():
            if action.isChecked():
                self._selectedOperators.append(action.text())
        self._pageNum = 1
        self.query_tasks(self._pageNum, self._pageSize,
                         task_states=self._selectedTaskStates,
                         operators=self._selectedOperators,
                         batch=self._selectedBatch,
                         tiff_name=self._selectedTiffName)

    def onShowDataEditFrame(self):
        visiable = self.dateEdit_frame.isVisible()
        self.dateEdit_frame.setVisible(not visiable)

        if not visiable:
            sty = self.sty
            pressed_sty = sty.replace('background:white','background:#F0F0F0')
            self.batch_button.setStyleSheet(pressed_sty)
        else:
            self.batch_button.setStyleSheet(self.sty)

    def onPrePage(self):
        if self._pageNum <= 1:
            self._pageNum = 1
            return
        self._pageNum = self._pageNum - 1
        self.query_tasks(self._pageNum, self._pageSize,
                         task_states=self._selectedTaskStates,
                         operators=self._selectedOperators,
                         batch=self._selectedBatch,
                         tiff_name=self._selectedTiffName)

    def onNextPage(self):
        if self._pageNum >= self._totalPage:
            self._pageNum = self._totalPage
            return
        self._pageNum = self._pageNum + 1
        self.query_tasks(self._pageNum, self._pageSize,
                         task_states=self._selectedTaskStates,
                         operators=self._selectedOperators,
                         batch=self._selectedBatch,
                         tiff_name=self._selectedTiffName)

    def onForwardToPage(self):
        pageNumText = self._pageNum_edit.text()
        if pageNumText:
            pageNum = int(pageNumText)
            if pageNum == self._pageNum or pageNum < 1 or pageNum > self._totalPage:
                return
            self._pageNum = pageNum
            self.query_tasks(self._pageNum, self._pageSize,
                             task_states=self._selectedTaskStates,
                             operators=self._selectedOperators,
                             batch=self._selectedBatch,
                             tiff_name=self._selectedTiffName)
        self._pageNum_edit.clear()
        self._pageNum_edit.clearFocus()
    def onSearch(self):
        searchKeyWord = self._search_edit.text()
        self._selectedTiffName = searchKeyWord
        self._pageNum = 1
        self.query_tasks(self._pageNum, self._pageSize,
                             task_states=self._selectedTaskStates,
                             operators=self._selectedOperators,
                             batch=self._selectedBatch,
                             tiff_name=self._selectedTiffName)
        self._search_edit.clearFocus()
    def onTimeout(self):
        self.query_tasks(self._pageNum, self._pageSize,
                         task_states=self._selectedTaskStates,
                         operators=self._selectedOperators,
                         batch=self._selectedBatch,
                         tiff_name=self._selectedTiffName)

    def changeQueryStatus(self, status: list):
        self._selectedTaskStates.clear()
        self._selectedTaskStates.extend(status)
        self._pageNum = 1
        self.query_tasks(self._pageNum, self._pageSize,
                         task_states=self._selectedTaskStates,
                         operators=self._selectedOperators,
                         batch=self._selectedBatch,
                         tiff_name=self._selectedTiffName)

    def setTitleText(self, text:str):
        self._titleLabel.setText(text)

    def eventFilter(self, watched: QObject, event:QEvent):

        if (watched == self.startDate or self.endDate) and event.type() == QEvent.Wheel:

            return True


        elif watched == self.window:

            if event.type()==event.MouseButtonRelease:

                if self.dateEdit_frame.isVisible():
                    self.dateEdit_frame.setVisible(False)
                    self.batch_button.setStyleSheet(self.sty)

        return False

class AdapterWidget(QWidget):

    def __init__(self, taskList, headerWidget: QWidget):
        super(AdapterWidget, self).__init__()
        self._taskList = taskList
        self._headerWidget = headerWidget
        self._itemlist = []
        self.vertical_layout = QVBoxLayout(self)   # 所有行content布局
        self.vertical_layout.setSpacing(0)
        self.vertical_layout.setMargin(0)
        self.vertical_layout.setAlignment(Qt.AlignTop)
        self.get_item()
    # 获取item为UI对象和task为查询到的模型类对象，传到fillItemData用
    def get_item(self):
        taskNum = len(self._taskList)
        itemNum = len(self._itemlist)
        count = taskNum if taskNum >= itemNum else itemNum
        for i in range(count):
            if taskNum >= itemNum:
                task = self._taskList[i]
                if i < itemNum:
                    item = self._itemlist[i]
                else:
                    item = ListItemWidget(headerWidget=self._headerWidget)
                    self._itemlist.append(item)
                    item.setFixedHeight(34) #设置行高
                    self.vertical_layout.addWidget(item)
                self.fillItemData(item, task)
            else:
                if i < taskNum:
                    task = self._taskList[i]
                    item = self._itemlist[i]
                    self.fillItemData(item, task)
                else:
                    item = self._itemlist[-1]
                    self.vertical_layout.removeWidget(item)
                    self._itemlist.remove(item)
                    item.close()

    # 填数据内容
    def fillItemData(self,item, task:models.Tasks):
        tiff_name = os.path.basename(task.tiffFile)  # 文件名保存的是觉对路径，这函数只取路径的文件名
        item.tiff_name_label.setText(tiff_name)
        item.project_label.setText(task.inspectionItem)
        item.chip_ver_label.setText(task.chipVer)
        item.batch_label.setText(task.batch)
        item.founder_label.setText(task.operator)
        item.logButton.tag = task.id
        item.reportButton.tag = task.id
        item.cancelButton.tag = task.id
        state = task.state
        if state == Constants.State.IDLE or state == Constants.State.QUEUING:
            item.analysis_state_label.setText("等待分析")
            item.analysis_state_label.setStyleSheet('font-size: 10px;color:#000000;')
            item.cancelButton.setEnabled(True)
            item.reportButton.setEnabled(False)
        elif state == Constants.State.EXTRACTING_DATA or state == Constants.State.ANALYSING_DATA :
            item.analysis_state_label.setText("正在分析")
            item.analysis_state_label.setStyleSheet('font-size: 10px;color:#000000;')
            item.cancelButton.setEnabled(False)
            item.reportButton.setEnabled(False)
        elif state == Constants.State.COMPLETE:
            item.analysis_state_label.setText("分析成功")
            item.analysis_state_label.setStyleSheet('font-size: 10px;color:#00C867;')
            item.cancelButton.setEnabled(False)
            item.reportButton.setEnabled(True)
        elif state == Constants.State.CANCEL:
            item.analysis_state_label.setStyleSheet('font-size: 10px;color:#000000;')
            item.analysis_state_label.setText("已取消")
            item.cancelButton.setEnabled(False)
            item.reportButton.setEnabled(False)
        elif state == Constants.State.FAILURE:
            item.analysis_state_label.setText("分析失败")
            item.analysis_state_label.setStyleSheet('font-size: 10px;color:#FF3A5F;')
            item.cancelButton.setEnabled(False)
            item.reportButton.setEnabled(False)


class MyPushButton(QPushButton):
    def __init__(self, text):
        super(MyPushButton, self).__init__(text=text)
        self.tag = None
        self.setFocusPolicy(Qt.NoFocus)

# 设置一行内容item
class ListItemWidget(QWidget):
    def __init__(self, headerWidget: QWidget):
        super(ListItemWidget, self).__init__()
        self.headerWidget = headerWidget
        self.init_itemUI()

    def init_itemUI(self):

        v_contentLayout = QVBoxLayout()# 内容外V布局（分割线+子布局itemLayout）
        v_contentLayout.setSpacing(0)
        v_contentLayout.setMargin(0)  # 无间距
        h_itemLayout = QHBoxLayout() # 项目H布局(文字label+按钮布局)
        h_itemLayout.setSpacing(0)
        h_itemLayout.setMargin(0)
        h_textLayout = QHBoxLayout()
        h_textLayout.setSpacing(0)
        h_textLayout.setMargin(0)
        h_handleLayout = QHBoxLayout()
        h_handleLayout.setSpacing(0)
        h_handleLayout.setMargin(0)

        self.tiff_name_label = QLabel("tiff1.tiff")
        self.tiff_name_label.setStyleSheet("font-size: 10px;color: #4A4A4A;")
        self.tiff_name_label.setFixedWidth(self.headerWidget.findChild(QLabel, "tiffLabel").width())
        self.chip_ver_label = QLabel("V13")
        self.chip_ver_label.setStyleSheet("font-size: 10px;color: #4A4A4A;")
        self.chip_ver_label.setFixedWidth(self.headerWidget.findChild(QLabel, "chipLabel").width())
        self.project_label = QLabel("肺结节良恶性分析")
        self.project_label.setStyleSheet("font-size: 10px;color: #4A4A4A;")
        self.project_label.setFixedWidth(self.headerWidget.findChild(QLabel, "projectLabel").width())
        self.founder_label = QLabel("李四")
        self.founder_label.setStyleSheet("font-size: 10px;color: #4A4A4A;")
        self.founder_label.setFixedWidth(self.headerWidget.findChild(QLabel, "founderLabel").width())
        self.batch_label = QLabel("20210330-1")
        self.batch_label.setStyleSheet("font-size: 10px;color: #4A4A4A;")
        self.batch_label.setFixedWidth(self.headerWidget.findChild(QLabel, "batchLabel").width())
        self.analysis_state_label = QLabel("等待分析")
        self.analysis_state_label.setFixedWidth(self.headerWidget.findChild(QLabel, "stateLabel").width())


        self.tiff_name_label.setObjectName("tiffNameLabel")
        self.chip_ver_label.setObjectName("chipVerLabel")
        self.project_label.setObjectName("projectLabel")
        self.founder_label.setObjectName("founderLabel")
        self.batch_label.setObjectName("batchLabel")
        self.analysis_state_label.setObjectName("analysisStateLabel")
        h_textLayout.addWidget(self.tiff_name_label, stretch=4)
        h_textLayout.addWidget(self.chip_ver_label,  stretch=2)
        h_textLayout.addWidget(self.project_label, stretch=3)
        h_textLayout.addWidget(self.founder_label, stretch=2)
        h_textLayout.addWidget(self.batch_label, stretch=2)
        h_textLayout.addWidget(self.analysis_state_label, stretch=2)

        #操作按钮设置
        bijiPath = BaseWidget.resourcePath("res/biji.png")
        cancelPath = BaseWidget.resourcePath("res/cancel.png")
        reportPath = BaseWidget.resourcePath("res/report.png")
        self.logButton = MyPushButton("")
        self.logButton.setToolTip('分析日志')
        self.logButton.clicked.connect(self._onLog)
        self.logButton.setStyleSheet("QToolTip{border:None;background-color:#FFFFFF;}")
        self.logButton.setIcon(QIcon(bijiPath))

        self.cancelButton = MyPushButton("")
        self.cancelButton.setToolTip("取消检验")
        self.cancelButton.setStyleSheet("QToolTip{border:None;background-color:#FFFFFF;}")
        self.cancelButton.setIcon(QIcon(cancelPath))

        self.cancelButton.setEnabled(False)
        self.cancelButton.clicked.connect(self._onCancel)

        self.reportButton = MyPushButton("")
        self.reportButton.setToolTip('检验报告')
        self.reportButton.setStyleSheet("QToolTip{border:None;background-color:#FFFFFF;}")
        self.reportButton.setIcon(QIcon(reportPath))


        self.reportButton.clicked.connect(self._onReport)

        self.logButton.setFixedSize(self.logButton.iconSize())
        self.cancelButton.setFixedSize(self.cancelButton.iconSize())
        self.reportButton.setFixedSize(self.reportButton.iconSize())
        operationWidget = QWidget()
        h_handleLayout.addWidget(self.logButton)
        h_handleLayout.addWidget(self.reportButton)
        h_handleLayout.addWidget(self.cancelButton)
        h_handleLayout.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        h_handleLayout.setSpacing(12)
        operationWidget.setLayout(h_handleLayout)
        operationWidget.setFixedWidth(self.headerWidget.findChild(QLabel, "operationLabel").width())

        #行分割线
        separate_label = QLabel()
        separate_label.setFixedHeight(1)
        separate_label.setStyleSheet("background-color: #EAEAEC;")

        # 整合布局
        h_itemLayout.addLayout(h_textLayout)
        h_itemLayout.addWidget(operationWidget)

        v_contentLayout.addLayout(h_itemLayout)
        v_contentLayout.addWidget(separate_label)

        self.setLayout(v_contentLayout)



    def _onLog(self):
        pushButon = self.sender()
        self._logActivity = AnalysisLogActivity(pushButon.tag)
        self._logActivity.show()

    def _onReport(self):
        pushButon = self.sender()
        taskId = pushButon.tag
        reportfileModel = models.Session().query(models.TaskReport).filter_by(taskId=taskId).first()

        if reportfileModel:
            report_path = reportfileModel.reportPath
            if os.path.exists(report_path):
                # os.system(report_path)
                # subprocess.Popen(report_path, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                # print("tasklistactivity:",report_path)
                QDesktopServices.openUrl(QUrl.fromLocalFile(report_path) )

            else:
                self.show_msg_box("查看的报告不存在，可能已被删除！")

    def _onCancel(self):
        self.pushButon = self.sender()
        self._cancelTip= CancelTips()
        btn_point = self.pushButon.mapToGlobal(QPoint(0,0))
        btnX = btn_point.x()
        btnY = btn_point.y()
        btnWidth = self.pushButon.width()
        tipWidth = self._cancelTip.window.width()
        tipHeight = self._cancelTip.window.height()
        tipX = btnX+btnWidth-tipWidth+30   # 箭头约到tip右边的距离
        tipY = btnY - tipHeight
        self._cancelTip.window.move(tipX,tipY)
        self._cancelTip.mysignal.Signal.connect(self.cancelTask)
        self._cancelTip.exec()
    def cancelTask(self,flag:bool):
        if flag:
            TaskManager.instance().cancel_task(self.pushButon.tag)
            self._cancelTip.close()

    def show_msg_box(self, text):
        msg_box = QMessageBox()
        msg_box.setWindowTitle("提示")
        msg_box.setText(text)
        msg_box.exec_()



#
# if __name__ == '__main__':
#     models.init_db()
#     models.init_admin()
#     app = QApplication(sys.argv)
#     loginActivity = TaskListActivity()
#     loginActivity.show()
#     app.exec_()

