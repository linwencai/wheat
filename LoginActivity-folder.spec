# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['wheat\\main\\LoginActivity.py'],
             pathex=['C:\\Users\\linwencai\\Envs\IVD\\Lib\\site-packages'],
             binaries=[],
             datas=[('wheat\\layout', 'layout'), ('wheat\\res', 'res')],
             hiddenimports=['PySide2.QtXml', 'skimage.filters.rank.core_cy_3d','sqlalchemy.sql.default_comparator'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='Peptide Array免疫诊断软件',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False,
          icon = "IVD.ico" )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='Peptide Array免疫诊断软件')
